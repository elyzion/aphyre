package jp.aphyre.core.error

import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import jp.aphyre.core.error.AphyreRestErrors.NotFound
import org.scalatest.{FreeSpec, Matchers}
import spray.json._

/**
  * Created by Berthold Alheit on 11/24/15.
  */
class RestErrorToHttpEntityConversionSpec extends FreeSpec with BaseErrorHandler with Matchers {

  "BaseRestError can be converted to an HttpEntity" - {
    "with debug information" in {
      val nf = NotFound()
      restErrorToHttpEntity(nf) === HttpEntity(
        ContentTypes.`application/json`,
        DebugErrorResponse(message = nf.message, cause = nf.cause).toJson.prettyPrint
      )
    }
    "without debug information" in {
      val nf = NotFound()
      restErrorToHttpEntity(nf)(false) === HttpEntity.Empty
    }
  }
}
