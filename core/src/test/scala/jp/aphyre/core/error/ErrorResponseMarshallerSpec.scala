package jp.aphyre.core.error

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import jp.aphyre.core.MarshallingSpecHelper
import jp.aphyre.core.error.AphyreRestErrors.NotFound
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FreeSpec, Matchers}

/**
  * Created by Berthold Alheit on 11/24/15.
  */
class ErrorResponseMarshallerSpec extends FreeSpec with BaseErrorHandler with Matchers with ScalaFutures {
  import scala.concurrent.ExecutionContext.Implicits.global
  "BaseRestError can be marshalled to an HttpEntity" - {
    "with all fields" in {
      val nf = NotFound()

      whenReady(Marshal(nf).to[HttpResponse]) { resp ⇒
        resp shouldEqual MarshallingSpecHelper.preToHttpResponse(nf)
      }
    }
    "with customized fields" in {
      val e = new Exception("a bad thing")
      val nf = NotFound(message = Some("bad things!"), cause = Some(e))

      whenReady(Marshal(nf).to[HttpResponse]) { resp ⇒
        resp shouldEqual MarshallingSpecHelper.preToHttpResponse(nf)
      }
    }
  }
}
