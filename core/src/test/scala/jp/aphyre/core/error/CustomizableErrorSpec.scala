package jp.aphyre.core.error

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import jp.aphyre.core.MarshallingSpecHelper
import jp.aphyre.core.json.Marshalling
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FreeSpec, Matchers}

/**
  * Created by Berthold Alheit on 11/24/15.
  */
class CustomizableErrorSpec extends FreeSpec with BaseErrorHandler with Matchers  with ScalaFutures with Marshalling {
  import scala.concurrent.ExecutionContext.Implicits.global
  "BaseRestError can be extended by framework users" in {
    case class MyError() extends
      BaseError(
        httpStatusCode = StatusCodes.InternalServerError,
        detailStatusCode = Some(100),
        message = Some("Horrible error"),
        cause = Some(new Exception("No users found for you, this should not happen."))
      )

    val me = MyError()

    whenReady(Marshal(me).to[HttpResponse]) { resp ⇒
      resp shouldEqual MarshallingSpecHelper.preToHttpResponse(me)
    }
  }
}
