package jp.aphyre.core.error

import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import jp.aphyre.core.module.DefaultModules
import jp.aphyre.core.{AphyreResults, RoutingSpec}
import scaldi.Injector

/**
  * Created by Berthold Alheit on 11/24/15.
  */
class ErrorSpec extends RoutingSpec with BaseErrorHandler {

  override implicit def appModule: Injector = DefaultModules.modules

  case class MyError() extends
    BaseError(
      httpStatusCode = StatusCodes.Conflict,
      detailStatusCode = Some(100),
      message = Some("Horrible error"),
      cause = Some(new Exception("No users found for you, this should not happen."))
    )

  "The application will process errors. " - {
    "Standard errors" - {
      "can be returned as a left and provides error details in the response body" ignore {
        val me = MyError()
        Get() ~> complete(AphyreResults.Ng(me)) ~> check {
          responseAs[DebugErrorResponse] === DebugErrorResponse(message = me.message, cause = me.cause)
        }
      }
      "only returns status codes when in prod mode" ignore {
        val me = MyError()
        Get() ~> complete(AphyreResults.Ng(me)) ~> check {
          responseAs[DebugErrorResponse] shouldEqual DebugErrorResponse(message = me.message, cause = me.cause)
        }
      }
      "returns status codes and body when in debuging prod mode" ignore {
        val me = MyError()
        Get() ~> complete(AphyreResults.Ng(me)) ~> check {
          responseAs[DebugErrorResponse] shouldEqual DebugErrorResponse(message = me.message, cause = me.cause)
        }
      }
      "can set status codes" in {
        val me = MyError()
        Get() ~> complete(AphyreResults.Ng(me)) ~> check {
          status === StatusCodes.Conflict
        }
      }
      "expose a detail-status-code header when set" in {
        val me = MyError()
        Get() ~> complete(AphyreResults.Ng(me)) ~> check {
          header(`detail-status-code`) shouldBe Some(RawHeader(`detail-status-code`, "100"))
        }
      }
      "can be returned as a right since they is marshallable" in {
        val me = MyError()
        Get() ~> complete(AphyreResults.Ok(me)) ~> check {
          status === StatusCodes.Conflict
          responseAs[String] should include ("Horrible error")
        }
      }
      "will leave log entries for standard errors" in {
        Get() ~> complete(throw new IllegalArgumentException()) ~> check {
          status === StatusCodes.InternalServerError
          responseAs[String] === "Unknown server-side error"
        }
      }
    }
    "Errors external to the framework/application logic" - {
      "will be processed and logged" in {
        Get() ~> complete(throw new IllegalArgumentException()) ~> check {
          status === StatusCodes.InternalServerError
          responseAs[String] === "Unknown server-side error"
        }
      }
    }
    "Critical errors such a JVM errors" - {
      "will be processed and logged" in {
        implicit val logger = Logging(actorSystem, this.getClass)
        Get() ~> get { handleExceptions(baseExceptionHandler) { complete(throw new RuntimeException()) } } ~> check {
          status === StatusCodes.InternalServerError
          responseAs[String] === "Critical server-side error encountered."
        }
      }
    }
  }
}
