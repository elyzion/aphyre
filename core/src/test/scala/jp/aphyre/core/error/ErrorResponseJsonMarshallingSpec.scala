package jp.aphyre.core.error

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import jp.aphyre.core.json.Marshalling
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FreeSpec, Matchers}
import spray.json._

/**
  * Created by Berthold Alheit on 11/24/15.
  */
class ErrorResponseJsonMarshallingSpec extends FreeSpec with Marshalling with Matchers with ScalaFutures with BaseErrorHandler {
  import scala.concurrent.ExecutionContext.Implicits.global

  "ErrorResponse based entities can be marshalled" - {
    "with all fields - ignored due to issue with equality comparator." in {
      val exception = new Exception("test")
      val errorResponse: DebugErrorResponse = DebugErrorResponse(message = Some("m"), cause = Some(exception))

      val body = HttpEntity(
        contentType = ContentTypes.`application/json`,
        string =
          JsObject(
            "message" -> JsString(errorResponse.message.get),
            "cause" -> JsObject(
              "message" → JsString(exception.getMessage),
              "stacktrace" → JsArray(exception.getStackTrace.map(entry ⇒ JsString(entry.toString)):_*)
            )
          ).compactPrint
      )

      whenReady(Marshal(errorResponse).to[HttpEntity]) { resp ⇒
        resp shouldEqual body
      }
    }
    "with missing fields" in {
      val exception = new Exception("test")
      val errorResponse: DebugErrorResponse = DebugErrorResponse(message = Some("m"), cause = None)

      val body = HttpEntity(
        contentType = ContentTypes.`application/json`,
        string =
          JsObject(
            "message" -> JsString(errorResponse.message.get)
          ).compactPrint
      )

      whenReady(Marshal(errorResponse).to[HttpEntity]) { resp ⇒
        resp shouldEqual body
      }
    }
  }
}
