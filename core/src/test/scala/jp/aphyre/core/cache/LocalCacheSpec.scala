package jp.aphyre.core.cache

import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FreeSpec, Matchers}

import scala.collection.concurrent.TrieMap
import scala.concurrent.{Await, Future}

/**
  * Basic tests for validation logic.
  */
class LocalCacheSpec extends FreeSpec with Matchers with ScalaFutures {

  /**
    * Define enumeration with types.
    */
  object NamedCaches extends Enumeration {
    type NamedCache = Value
    val TEST_KEY =  Value("TEST_KEY")
    val SECOND_TEST_KEY =  Value("SECOND_TEST_KEY")
  }

  case class TestValue(id: Int, value: String)
  case class SecondTestValue(id: Int, value: String)
  val stringCacheName = "Another"

  /**
    * Manages named caches. Named caches can be reset and reloaded manually using the API.
    * Use named caches for data that might need to be reloaded from time to time, without a server reboot.
    * Examples of this would be game master data, ie, level data or reward data.
    */
  class CacheManager extends LocalCache {
    /** This map defines *everything* we require to build our caches. */
    override val cacheable = TrieMap(
      NamedCaches.TEST_KEY → new Initializer[TestValue] {
        /** Tells us what to use to index in our cache. */
        override def index(o: TestValue): Any = o.id
        /** Tells us what to cache. */
        override def sources: List[TestValue] = List(TestValue(1, "1"))
      },
      NamedCaches.SECOND_TEST_KEY → new Initializer[SecondTestValue] {
        override def index(o: SecondTestValue): Any = o.id
        override def sources: List[SecondTestValue] = List(SecondTestValue(2, "2"), SecondTestValue(3, "3"))
      },
      stringCacheName → new Initializer[SecondTestValue] {
        override def index(o: SecondTestValue): Any = o.id
        override def sources: List[SecondTestValue] = List(SecondTestValue(2, "2"), SecondTestValue(3, "3"))
      }
    )
  }

  "LocalCache" - {
    "initaliazes values when calling init" in {
      val cache = new CacheManager()
      cache.caches.size === 0
      cache.load()
      cache.caches.size === 2
    }
    "initializes caches on first access if not initialized yet" in {
      val cache = new CacheManager()
      cache.caches.size === 0
      cache(NamedCaches.TEST_KEY).get(1) === Some(TestValue(1, "1"))
      cache.caches.size === 1
    }
    "can access cache items" in {
      val cache = new CacheManager()
      //can access via get
      cache(NamedCaches.SECOND_TEST_KEY).get(2) === Some(TestValue(2, "2"))
      //Can access via apply
      cache(NamedCaches.SECOND_TEST_KEY)(2) === Some(TestValue(2, "2"))
    }
    "can store caches for various keys" in {
      val cache = new CacheManager()
      // An enum value
      cache(NamedCaches.SECOND_TEST_KEY).get(2) === Some(TestValue(2, "2"))
      // A string value
      cache(stringCacheName)(2) === Some(TestValue(2, "2"))
    }
    "returns none for non-existing items" in {
      val cache = new CacheManager()
      cache(NamedCaches.SECOND_TEST_KEY).get(6) === None
    }
    "can be updated by calling load" in {
      var testValues: List[TestValue] = List(TestValue(1, "1"))
      val cache = new LocalCache {
        override val cacheable = TrieMap(
          NamedCaches.TEST_KEY → new Initializer[TestValue] {
            def index(o: TestValue): Any = o.id
            def sources: List[TestValue] = testValues
            def cacheName: NamedCaches.NamedCache = NamedCaches.TEST_KEY
          }
        )
      }

      cache.load()
      cache(NamedCaches.TEST_KEY)(1) === Some(TestValue(1, "1"))
      cache(NamedCaches.TEST_KEY)(7) shouldBe None
      testValues = List(TestValue(7, "7"))
      cache.load()
      cache(NamedCaches.TEST_KEY)(1) shouldBe None
      cache(NamedCaches.TEST_KEY)(7)  === Some(TestValue(7, "7"))
    }
    "can be accessed as a map" in {
      val cache = new CacheManager()
      cache.load()
      //Directly access a *copy* of the internal TrieMap
      cache(NamedCaches.TEST_KEY)() shouldBe a [Map[_, TestValue]]
      //The above apply() aliases the map call.
      cache(NamedCaches.TEST_KEY).map shouldBe a [Map[_, TestValue]]
      //MapLike access.
      cache(NamedCaches.SECOND_TEST_KEY)(2) === Some(TestValue(2, "2"))
    }
    "can be accessed easily be creating a wrapper object" in {
      object AnotherCache extends LocalCache {
        override val cacheable = TrieMap(
          NamedCaches.TEST_KEY → new Initializer[TestValue] {
            override def index(o: TestValue): Any = o.id
            override def sources: List[TestValue] = List(TestValue(1, "1"))
          }
        )
      }
      //can access via get
      AnotherCache(NamedCaches.TEST_KEY).get(1) === Some(TestValue(1, "1"))
      //Can access via apply
      AnotherCache(NamedCaches.TEST_KEY)(1) === Some(TestValue(1, "1"))
    }
    "can added by calling addCacheable" in {
      object AnotherCache extends LocalCache
      AnotherCache.addCacheable(NamedCaches.TEST_KEY, new Initializer[TestValue] {
        override def index(o: TestValue): Any = o.id
        override def sources: List[TestValue] = List(TestValue(1, "1"))
      })
      //can access via get
      AnotherCache(NamedCaches.TEST_KEY).get(1) === Some(TestValue(1, "1"))
      //Can access via apply
      AnotherCache(NamedCaches.TEST_KEY)(1) === Some(TestValue(1, "1"))
    }
    "concurrency checking" in {
      object CurrencyTestCacheManager extends LocalCache {
        // This map defines *everything* we require to build our caches.
        override val cacheable = TrieMap(
          NamedCaches.TEST_KEY → new Initializer[TestValue] {
            // Tells us what to use to index in our cache.
            override def index(o: TestValue): Any = o.id
            // Tells us what to cache.
            override def sources: List[TestValue] = List(TestValue(1, "1"))
          },
          NamedCaches.SECOND_TEST_KEY → new Initializer[SecondTestValue] {
            override def index(o: SecondTestValue): Any = o.id
            override def sources: List[SecondTestValue] = List(SecondTestValue(2, "2"), SecondTestValue(3, "3"))
          },
          stringCacheName → new Initializer[SecondTestValue] {
            override def index(o: SecondTestValue): Any = o.id
            override def sources: List[SecondTestValue] = List(SecondTestValue(2, "2"), SecondTestValue(3, "3"))
          }
        )
      }
      import scala.concurrent.ExecutionContext.Implicits.global
      val futures = Range(0,200).map { i =>
        if (i >=180) {
          Future {
            for (_ <- Range(0, 100)) { _: Int =>
              CurrencyTestCacheManager.load()
            }
          }
        } else {
          Future {
            for (_ <- Range(0, 100)) { _: Int =>
              //can access via get
              CurrencyTestCacheManager(NamedCaches.TEST_KEY).get(1) === Some(TestValue(1, "1"))
              //can access via get
              CurrencyTestCacheManager(NamedCaches.SECOND_TEST_KEY).get(2) === Some(TestValue(2, "2"))
            }
          }
        }
      }

      import scala.concurrent.duration._
      Await.result(Future.sequence(futures), 1 minute)
    }

  }
}