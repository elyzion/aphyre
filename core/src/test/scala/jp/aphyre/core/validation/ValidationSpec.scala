package jp.aphyre.core.validation

import org.scalatest.{FreeSpec, Matchers}

/**
  * Basic tests for validation logic.
  */
class ValidationSpec extends FreeSpec with Matchers {

  import spray.json.DefaultJsonProtocol._
  import spray.json._

  case class Tester(id: Int, name: String) extends Validating {
    addValidation(Option(id).isDefined, "id_required", "The id is required.")
    addValidation(Option(id).isDefined && id > 0, "id_lg_zero", "The should be larger than 0")
    addValidation((name ne null) && !name.isEmpty, "name_required", "The name is required.")
  }

  "The validation interface" - {
    "for an invalid class" - {
      "returns json formatted errors wrapped in an exception" in {
        intercept[IllegalArgumentException] {
          Tester(-1, null)
        } match {
          case e: IllegalArgumentException => {
            val messages = e.getMessage.stripPrefix("requirement failed: ")
            val m = messages.parseJson.convertTo[Map[String, String]]
            m.contains("id_lg_zero") && m.contains("name_required") shouldBe true
          }
        }
      }
      "returns all validation failures" in {
        intercept[IllegalArgumentException] {
          Tester(-1, null)
        } match {
          case e: IllegalArgumentException => {
            val messages = e.getMessage.stripPrefix("requirement failed: ").parseJson.convertTo[Map[String, String]]
            messages.keys.size shouldEqual 2
          }
        }
      }
    }
    "returns no errors for a valid class" in {
      Tester(1, "Something")
    }
  }
}
