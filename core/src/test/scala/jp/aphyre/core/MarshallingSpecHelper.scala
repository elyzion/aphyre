package jp.aphyre.core

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers.RawHeader
import jp.aphyre.core.error.{BaseErrorHandler, BaseError}

/**
  * Created by elyzion on 4/27/16.
  */
object MarshallingSpecHelper extends BaseErrorHandler {
  def preToHttpResponse(value: BaseError): HttpResponse = {
    val detailHeader = value.detailStatusCode map { t ⇒ List(RawHeader(`detail-status-code`, t.toString)) } getOrElse { List.empty }
    HttpResponse(status = value.httpStatusCode, entity = value, headers = detailHeader)
  }
}
