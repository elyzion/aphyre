package jp.aphyre.core

import akka.actor.ActorSystem
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest._
import scaldi.Injector
import scaldi.akka.AkkaInjectable
import akka.http.scaladsl.server.Directives._

/**
  * Base class for testing framework directives.
  */
abstract class RoutingSpec extends FreeSpec with Matchers  with ScalatestRouteTest with AkkaInjectable  {

  val Ok = HttpResponse()
  val completeOk = complete(Ok)

  def echoComplete[T]: T ⇒ Route = { x ⇒ complete(x.toString) }
  def echoComplete2[T, U]: (T, U) ⇒ Route = { (x, y) ⇒ complete(s"$x $y") }

  implicit def appModule: Injector
  implicit def actorSystem: ActorSystem = system
  def actorRefFactory = system
}
