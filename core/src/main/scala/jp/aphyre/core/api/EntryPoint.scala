package jp.aphyre.core.api

import akka.http.scaladsl.server.{Directives, Route, RouteConcatenation}

/**
  * Trait for base router. The application entry point needs to implement this.
  */
trait EntryPoint extends RouteConcatenation {
  def route: Route
}

/**
  * The base null router. This router is bound by default, if the user does not bind a router, then it will display it's message.
  */
class NullEntryPoint extends EntryPoint with Directives {
  override val route: Route = complete("Please implement the router interface.")
}

