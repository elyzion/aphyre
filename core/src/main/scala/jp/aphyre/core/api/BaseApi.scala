package jp.aphyre.core.api

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}
import jp.aphyre.core.error.BaseErrorHandler
import jp.aphyre.core.json.Marshalling
import scaldi.Injector
import scaldi.akka.AkkaInjectable

import scala.concurrent.ExecutionContextExecutor

/**
  * Basic abstract class for creating an API.
  *
  * Extend this in your own project, either directly or after extending it with your own abstract class and settings.
  */
abstract class BaseApi(implicit inj: Injector) extends AkkaInjectable with Directives with Marshalling with BaseErrorHandler {
  val config: Config = ConfigFactory.load().withFallback(ConfigFactory.load("reference.conf")).resolve()

  /**
    * We are using an actor, so we require and implicit actor system and executionContext in scope.
    */
  implicit val system: ActorSystem = inject[ActorSystem]
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  /**
    * We set the timeout for operations in this api.
    */
  import scala.concurrent.duration._

  val duration: FiniteDuration = Duration(config.getInt("application.server.timeout"), SECONDS)
  implicit val timeout: Timeout = Timeout.durationToTimeout(duration)
}
