package jp.aphyre.core.validation

/**
  * Validation trait.
  *
  * Uses DelayedInit to execute validation after class instantiation.
  * DelayedInit is deprecated pending completion of https://issues.scala-lang.org/browse/SI-4330
  */
trait Validating extends DelayedInit {
  import spray.json.DefaultJsonProtocol._
  import spray.json._

  var validations: List[Validation] = List[Validation]()

  /**
    * Creates a new validation and adds it to the validation list.
    * @param p
    * @param key
    * @param message
    */
  def addValidation(p: => Boolean, key: String,  message: String = "") = {
    validations = new Validation(() => p, key, message) :: validations
  }

  /**
    * Adds the provided validation to the validation list.
    * @param validation
    */
  def addValidation(validation: Validation) = {
    validations = validation :: validations
  }

  /**
    * Returns a list of (json formatted) errors.
    *
    * TODO: this should probably just return a list and leave the json bit to the marshaller.
    * @return
    */
  def getErrors(): Option[String] = {
    validations.nonEmpty match {
      case true =>
        Some((validations.filter(validation => !validation.predicate())
          .map(validation => validation.key -> validation.message)(collection.breakOut): Map[String, String]).toJson.prettyPrint)
      case false => None
    }
  }

  /**
    * Executed the validation.
    */
  def validate(): Unit = {
    require(
      isValid(),
      getErrors().getOrElse("")
    )
  }

  /**
    * Returns true if the object is valid. Otherwise false.
    *
    * @return
    */
  def isValid(): Boolean = {
    !validations.exists(validation => !validation.predicate())
  }

  /**
    * This invokes the validation function after all instantiation, including that of the implementing class, is completed.
    * @param body
    */
  def delayedInit(body: => Unit) = {
    body
    validate()
  }
}

/** Validation wrapper */
case class Validation(var predicate: () => Boolean, key: String, message: String = "")
