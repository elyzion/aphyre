package jp.aphyre.core.actor.helper

import akka.actor.Actor
import akka.pattern._

import scala.concurrent._
import jp.aphyre.core.error.AphyreRestErrors.InternalServerError
import jp.aphyre.core.error.RestErrorImplicits

/**
  * Created by elyzion on 11/6/15.
  */
trait AnswerSupport extends RestErrorImplicits {
  this: Actor ⇒

  /**
    * Handle exceptions in one place. If we don't handle exception explicitly,
    * the actor will terminate without returning, this would cause a timeout exception to be
    * raised in the case of an ask invocation.
    *
    * @param result
    * @tparam A
    */
  def answerWith[A](result: ⇒ A): Unit = {
    try {
      /** We can add trace segments for kamon like this:
      sender() ! Tracer.currentContext.withNewSegment("actor-trace", "akka-actor", "aphyre") {
        result
      }
      */
      sender() ! result
    } catch {
      case e: Exception ⇒
        // FIXME: Completing with success and returning a rest-error might be a better response.
        sender() ! akka.actor.Status.Failure(InternalServerError(s"An exception occurred inside an actor:\t ${e.getMessage}", e))
        throw e
    }
  }

  def pipeAnswer[A](result: => Future[A])(implicit ec: ExecutionContext): Unit = {
    try {
      /** We can add trace segments for kamon like this:
      val r = Tracer.currentContext.withNewSegment("actor-trace", "akka-actor", "aphyre") {
        result
      }
      */
      pipe(result) to sender()
    } catch {
      case e: Exception ⇒
        // FIXME: Completing with success and returning a rest-error might be a better response.
        sender() ! akka.actor.Status.Failure(InternalServerError(s"An exception occurred inside an actor:\t ${e.getMessage}", e))
        throw e
    }

  }
}

