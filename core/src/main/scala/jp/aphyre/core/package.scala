package jp.aphyre

import jp.aphyre.core.error.BaseError

package object core {

  /**
    * Instantiate the basic response trait using the default error code provider.
    */
  object AphyreResults {
    type AphyreResult[A] = Either[BaseError, A]
    val Ok = Right
    val Ng = Left
  }
}
