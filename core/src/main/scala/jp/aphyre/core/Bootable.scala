package jp.aphyre.core

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import jp.aphyre.core.api.EntryPoint
import jp.aphyre.core.db.DatabaseProvider
import jp.aphyre.core.module.DefaultModules
import kamon.Kamon
import org.slf4j.{Logger, LoggerFactory}
import scaldi.Injector
import scaldi.akka.AkkaInjectable
import scaldi.akka.AkkaInjectable._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

/**
  * This class represent the application itself.
  *
  * Creating a new instance of application server will execute the logic defined in the Server trait.
  *
  * @param actorSystem
  * @param appModule
  */
class ApplicationServer(implicit val actorSystem: ActorSystem,
                        implicit val dispatcher: ExecutionContextExecutor,
                        implicit val materializer: ActorMaterializer,
                        implicit val appModule: Injector) extends Server

/**
  * Entry point of application.
  * - Defines the actor system for this application.
  * - Creates the server instance.
  * - adds the shutdown hook for the actor system.
  */
trait Bootable extends AkkaInjectable {
  this: Configurable =>
  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  // Kamon configuration value.
  val startKamon: Boolean = ConfigFactory.load().getBoolean("application.kamon.start")
  if (startKamon) {
    // Startup kamon monitoring
    logger.info("********** Starting monitoring system (kamon) ************")
    Kamon.start()
  }

  // Scaldi modules are prioritized from left to right, it is possible to override everything from the user provided config object.
  logger.info("********** Loading Scaldi injector system ************")
  implicit val appModule: Injector = module :: DefaultModules.modules
  implicit val actorSystem: ActorSystem = inject [ActorSystem]
  implicit val dispatcher: ExecutionContextExecutor = actorSystem.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  logger.info(s"********** Injected base actor system with name ${actorSystem.name} ************")

  /*
   * Inject database to initialize it.
   * It is possible to use non-lazy bindings, but that causes multiple initialization during unit tests.
   */
  val db: DatabaseProvider = inject [DatabaseProvider]
  logger.info(s"********** Injected database provider ${db.getClass.getName} ************")

  def main(args: Array[String]): Unit = {
    //Create the application server
    logger.info("********** Executing onInit ************")
    onInit()
    logger.info("********** Starting application server ************")
    val server = new ApplicationServer
    //Register actor system shutdown hook with the scala shutdown hook.
    sys.addShutdownHook({
      logger.info("********** Executing onShutdown ************")
      onShutdown()
      server.serverBinding.flatMap { s ⇒
        logger.info("********** Shutting Down application server ************")
        s.unbind() } onComplete { t ⇒
        logger.info("********** Shutting down actor system ************")
        Await.result(actorSystem.terminate(), 30 seconds)
      }
      if (startKamon) {
        logger.info("********** Shutting down kamon system ************")
        Kamon.shutdown()
      }
    })
  }

  /**
    * Override this to add additional init behaviours.
    * Executed before instantiation of ApplicationServer object, and after init of actor systems and db.
    */
  def onInit(): Unit = {}

  /**
    * Override this to add addition shutdown behaviours.
    */
  def onShutdown(): Unit = {}
}

/**
  * Starts up the spray-can http server.
  *
  * Requires the Bootstrap trait.
  */
trait Server {
  import jp.aphyre.core.error.BaseErrorHandler._
  import akka.http.scaladsl.Http
  import akka.http.scaladsl.server.Directives._
  import akka.stream.ActorMaterializer
  import scaldi.akka.AkkaInjectable._

  //Uses the implicits from Bootable or the test helper
  implicit val actorSystem: ActorSystem
  implicit val appModule: Injector
  implicit val materializer: ActorMaterializer
  implicit val dispatcher: ExecutionContextExecutor

  final val startupTimeout = inject [Int] ("application.server.startupTimeout" is by default 15)
  implicit val timeout: Timeout = Timeout(startupTimeout, TimeUnit.SECONDS)

  implicit val logger: LoggingAdapter = Logging(actorSystem.eventStream, this.getClass.toString)

  val host: String = inject [String] ("application.server.host" is by default "127.0.0.1")
  val port: Int = inject [Int] ("application.server.port" is by default 8080)
  val name: String = inject [String] ("application.name" is by default "MyApplication")
  val entryPoint: EntryPoint = inject [EntryPoint]

  val route: Route = handleExceptions(baseExceptionHandler) { entryPoint.route }

  logger.info(s"********** Starting application named $name on $host:$port ********** ")
  val serverBinding: Future[ServerBinding] = Http().bindAndHandle(Route.handlerFlow(route), host, port)

  serverBinding.failed.foreach {
    case ex: Exception =>
      logger.error(ex, "**********  Failed to bind to {}:{}! ********** ", host, port)
  }
}

