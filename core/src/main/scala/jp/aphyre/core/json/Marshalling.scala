package jp.aphyre.core.json

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling._
import jp.aphyre.core.json.helpers.AphyreJsonProtocols
import spray.json.DefaultJsonProtocol

/**
  * Main trait for marshalling support
  */
trait Marshalling extends DefaultJsonProtocol
  with SprayJsonSupport
  with PredefinedToResponseMarshallers
  with PredefinedToEntityMarshallers
  with GenericMarshallers
  with AphyreJsonProtocols