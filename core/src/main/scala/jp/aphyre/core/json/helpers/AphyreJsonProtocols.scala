package jp.aphyre.core.json.helpers

import java.util.UUID

import akka.http.scaladsl.model.StatusCode
import jp.aphyre.core.error.DebugErrorResponse
import org.joda.time
import org.joda.time.format.DateTimeFormat
import spray.json.DefaultJsonProtocol
import spray.json._

/**
  * Provides implicit converters for spray-json.
  * Any non-standard types need to be implemented like this.
  */
trait AphyreJsonProtocols extends DefaultJsonProtocol {

  /**
    * UUID formatter for Spray-Json
    */
  implicit object UUIDJsonFormat extends JsonFormat[UUID] {
    def write(x: UUID) = {
      require(x ne null)
      JsString(x.toString)
    }
    def read(value: JsValue) = value match {
      case JsString(x) => UUID.fromString(x)
      case x => deserializationError("Expected UUID as JsString, but got " + x)
    }
  }

  /**
    * Exception formatter for Spray-Json
    */
  implicit object BasicExceptionJsonFormat extends JsonFormat[Exception] {
    def write(x: Exception) = {
      require(x ne null)
      JsString(x.getMessage)
    }
    def read(value: JsValue) = value match {
      case JsString(x) => new Exception(x)
      case x => deserializationError("Expected exception as JsString, but got " + x)
    }
  }

  /**
    * Spray HTTP status code formatter for Spray-Json
    */
  implicit object StatusCodeJsonFormat extends JsonFormat[StatusCode] {
    def write(x: StatusCode) = {
      require(x ne null)
      JsNumber(x.intValue)
    }
    def read(value: JsValue) = value match {
      case JsNumber(x) =>
        try {
          StatusCode.int2StatusCode(x.toInt)
        } catch {
          case e: NoSuchElementException => deserializationError(s"${x} was not a status code")
        }
      case x => deserializationError("Expected StatusCode as JsNumber, but got " + x)
    }
  }

  /**
    * Spray HTTP status code formatter for Spray-Json
    */
  implicit object ThrowableFormat extends RootJsonFormat[Throwable] {
    def write(x: Throwable) = {
      require(x ne null)
      JsObject(
        "message" → JsString(x.getLocalizedMessage),
        "stacktrace" → JsArray(x.getStackTrace.map(entry ⇒ JsString(entry.toString)):_*)
      )
    }
    def read(value: JsValue) = throw new UnsupportedOperationException
  }

  /**
    * Provides the optional formatter for UUIDs.
 *
    * @return
    */
  implicit def optionThrowableFormat = new OptionFormat[Throwable]

  /**
    * Provides the optional formatter for UUIDs.
 *
    * @return
    */
  implicit def optionUUIDFormat = new OptionFormat[UUID]

  /**
    * Provides the optional formatter for Strings.
 *
    * @return
    */
  implicit def optionStringFormat = new OptionFormat[String]

  /**
    * Provides the optional formatter for DateTime.
 *
    * @return
    */
  //implicit def optionDateTimeFormat = new OptionFormat[time.DateTime]

  /**
    * Provides the formatting for the default error type.
    *
    * @return
    */
  implicit def errorResponseFormat = jsonFormat2(DebugErrorResponse)
}

/**
  * Companion object for the AphyreJsonProtocols trait.
  *
  * Import this if you don't want to extend the trait.
  */
object AphyreJsonProtocols extends AphyreJsonProtocols
