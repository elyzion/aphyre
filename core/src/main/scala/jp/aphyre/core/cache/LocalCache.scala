package jp.aphyre.core.cache

import java.util.concurrent.locks.StampedLock

import org.slf4j.{Logger, LoggerFactory}

import scala.collection.concurrent.TrieMap

/**
  * Managed caching. Define a enumeration with cache key indexes.
  *
  * Define your cache keys.
  * {{{
  *   object NamedCaches extends Enumeration {
  *     type NamedCache = Value
  *     val TEST_KEY =  Value("TEST_KEY")
  *   }
  *
  * }}}
  *
  * The we can create an instance of the heap cache.
  * We need to pass it a map detailing the caches it will load.
  *
  * {{{
  *   case class TestValue(id: Int, value: String)
  *   case class SecondTestValue(id: Int, value: String)

  *   object CacheManager extends NamedHeapCache[NamedCache] {
  *     /** This map defines *everything* we require to build our caches. */
  *     override val cacheable: Map[NamedCache, Initializer[_]] = Map(
  *       NamedCaches.TEST_KEY → new Initializer[TestValue] {
  *         /** Tells us what to use to index in our cache. */
  *         override def index(o: TestValue): Any = o.id
  *         /** Tells us what to cache. */
  *         override def sources: List[TestValue] = List(TestValue(1, "1"))
  *       },
  *       "AnotherKey" → new Initializer[SecondTestValue] {
  *         override def index(o: SecondTestValue): Any = o.id
  *         override def sources: List[SecondTestValue] = List(SecondTestValue(2, "2"), SecondTestValue(3, "3"))
  *       }
  *     )
  *   }
  * }}}
  *
  * Additionally, caches might be better names as loadedCaches, activeCaches or whatever.
  */
trait LocalCache {
  private[cache] var caches: TrieMap[Any, Cache[_]]  = TrieMap.empty[Any, Cache[_]]
  private[cache] val stampedLock: StampedLock = new StampedLock()

  /**
    * Asynchronous cache call.
    *
    * @param cache
    * @tparam A
    * @return
    */
  def apply[A](cache: Any): Cache[A] = {
    val optimisticLock = stampedLock.tryOptimisticRead()
    val cacheInstance = caches
      .getOrElseUpdate(cache, initialize(cache))
      .asInstanceOf[Cache[A]]
    if (!stampedLock.validate(optimisticLock)) {
      val readLock = stampedLock.readLock()
      try {
        caches
          .getOrElseUpdate(cache, initialize(cache))
          .asInstanceOf[Cache[A]]
      } finally stampedLock.unlock(readLock)
    } else {
      cacheInstance
    }
  }

  /**
    * Initializes all caches and loads cache data.
    */
  def load(): Unit = synchronized {
    val tmpCache = cacheable.keySet.map(key ⇒ (key, initialize(key))).toSeq
    val writeLock = stampedLock.writeLock()
    try {
      caches = TrieMap(tmpCache:_*)
    } finally stampedLock.unlock(writeLock)
  }

  /**
    * Initializes a specific and loads data for it.
    *
    * @param cache
    */
  def load(cache: Any): Unit = synchronized {
    caches.put(cache, initialize(cache))
  }

  private[cache] def initialize(cache: Any): Cache[_]  = {
    cacheable.get(cache) match {
      //TODO: move the init logic here... will require some generics/reflection.
      case Some(c) ⇒ c.load()
      case None ⇒ throw new IllegalArgumentException(s"No initializer registered for cache ${cache.getClass.getCanonicalName.toString}")
    }
  }

  /**
    * Contains a map of cache initializers.
    */
  val cacheable: TrieMap[Any, Initializer[_]] = TrieMap.empty

  /**
    * Adds a new initializer. Uses putIfAbsent since initializers should on by called once.
    * @param key The cache key.
    * @param initializer The initializer for the given cache.
    * @return None if set properly, otherwise returns the initializer located at the key.
    */
  def addCacheable(key: Any, initializer: Initializer[_]): Option[Initializer[_]] = {
    cacheable.putIfAbsent(key, initializer)
  }

  /**
    * Clears all registered caches in this LocalCache instance.
    */
  def clear(): Unit = caches.clear()

  /**
    * Clears the specified cache only.
    * @param cache
    */
  def clear(cache: Any): Unit = caches.remove(cache)
}

trait Initializer[V] {
  /**
    * Logic to create the key for an entry in the cache map.
    *
    * @param o the element
    * @return the key
    */
  def index(o: V): Any

  /**
    * The elements to be cached, to be returned as an list.
    * @return
    */
  def sources: List[V]

  /**
    * Creates a Cache[V] instance. This instance is stored
    * with the corresponding key from the cacheable map.
    *
    * FIXME: would be nice if we could move this to parent class.
    */
  def load(): Cache[V] = {
    val _cache = new DefaultCache[V]
    sources.foreach { source: V =>
      _cache(index(source), source)
    }
    _cache
  }
}

/**
  * General interface implemented for cache implementations.
  */
trait Cache[V] { cache ⇒

  /**
    * Provides a copy of the internal cache map.
    * Allows us to directly chain calls to the internal map.
    * doing things like cache(MyCache)(1) will call the apply(key) function from Map,
    * and not from the the cache instance.
    */
  def apply(): Map[Any, V]

  /**
    * Returns the elements located at key, or None.
    */
  def apply(key: Any): Option[V]

  /**
    * Returns the set of keys in the cache, in no particular order
    */
  def keys: Set[Any]

  /**
    * Provides a copy of the internal cache map.
    *
    * @return
    */
  def map: Map[Any, V]

  /**
    * Returns the Option wrapped element located at key
    */
  def get(key: Any): Option[V]

  /**
    * Returns the upper bound for the number of currently cached entries.
    * Note that this number might not reflect the exact number of active, unexpired
    * cache entries, since expired entries are only evicted upon next access
    * (or by being thrown out by a capacity constraint).
    */
  def size: Int

  /**
    * Sets the element located at key.
    * Returns None if there was no elements there.
    * Returns Some(V) if there was already an element there.
    */
  private[cache] def apply(key: Any, genValue: () ⇒ V): Option[V]

  /**
    * Sets the element located at key.
    * Returns None if there was no elements there.
    * Returns Some(V) if there was already an element there.
    */
  private[cache] def apply(key: Any, value: V): Option[V]

  /**
    * Removes the cache item for the given key. Returns the removed item if it was found (and removed).
    */
  private[cache] def remove(key: Any): Option[V]

  /**
    * Clears the cache by removing all entries.
    */
  private[cache] def clear()

  /**
    * Sets the element located at key.
    * Returns None if there was no elements there.
    * Returns Some(V) if there was already an element there.
    */
  private[cache] def put(key: Any, value: V): Option[V]

  /**
    * Sets the element located at key.
    * Returns None if there was no elements there.
    * Returns Some(V) if there was already an element there.
    */
  private[cache] def put(key: Any, genValue: () ⇒ V): Option[V]
}

/**
  * A basic thread-safe implementation of [[Cache]] backed by a TrieMap.
  */
final class DefaultCache[V] extends Cache[V] {
  private val store: TrieMap[Any, V]  = TrieMap.empty

  def apply(): Map[Any, V] = map

  def map: Map[Any, V] = store.toMap

  def apply(key: Any): Option[V] = get(key)

  def get(key: Any): Option[V] = store.get(key)

  def keys: Set[Any] = store.keySet.toSet

  def size: Int = store.size

  private[cache] def apply(key: Any, genValue: () ⇒ V): Option[V] = {
    put(key, genValue())
  }

  private[cache] def put(key: Any, value: V): Option[V] = {
    store.putIfAbsent(key, value)
  }

  private[cache] def apply(key: Any, value: V): Option[V] = {
    put(key, value)
  }

  private[cache] def put(key: Any, genValue: () ⇒ V): Option[V] = {
    store.putIfAbsent(key, genValue())
  }

  private[cache] def remove(key: Any): Option[V] = store.remove(key)

  private[cache] def clear(): Unit = { store.clear() }
}