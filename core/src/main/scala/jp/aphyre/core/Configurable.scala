package jp.aphyre.core

import com.typesafe.config.ConfigFactory
import scaldi.Injector

/**
  * Base class for accessing the standard typesafe config.
  */
trait TypeSafeConfig {
  val config = ConfigFactory.load().resolve()
}

object TypeSafeConfig extends TypeSafeConfig

/**
  * Base class for user provided configurations for scaldi.
  * Can also override typesafe configuration used here.
  */
trait Configurable extends TypeSafeConfig {
  /**
    * Creates and returns other binding and modules as required.
    * @return
    */
  def module: Injector
}
