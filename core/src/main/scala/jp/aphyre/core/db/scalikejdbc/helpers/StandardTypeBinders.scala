package jp.aphyre.core.db.scalikejdbc.helpers

import java.sql.ResultSet
import java.util.UUID

import scalikejdbc.TypeBinder

/**
  * Binder used to convert standard database types to Scala types.
  * Custom data types for scalikejdbc need to be implemented here.
  *
  * Database specific types should be implemented in separate traits.
  */
trait StandardTypeBinders {
  /**
    * Automatically binds UUID types from results sets
    */
  implicit val uuidTypeBinder: TypeBinder[UUID] = new TypeBinder[UUID] {
    def apply(rs: ResultSet, label: String): UUID = UUID.fromString(rs.getString(label))
    def apply(rs: ResultSet, index: Int): UUID = UUID.fromString(rs.getString(index))
  }

  /**
    * Automatically binds optional UUID types from results sets
    */
  implicit val uuidOptTypeBinder: TypeBinder[Option[UUID]] = new TypeBinder[Option[UUID]] {
    def apply(rs: ResultSet, label: String): Option[UUID] = Some(rs.getString(label)) match {
      case Some(uuid: String) => Some(UUID.fromString(uuid))
      case _ => None
    }

    def apply(rs: ResultSet, index: Int): Option[UUID] = Some(rs.getString(index)) match {
      case Some(uuid) => Some(UUID.fromString(uuid))
      case _ => None
    }
  }
}
