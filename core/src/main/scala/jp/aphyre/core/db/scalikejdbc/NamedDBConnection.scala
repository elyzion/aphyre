package jp.aphyre.core.db.scalikejdbc

import java.sql.Connection

import jp.aphyre.core.TypeSafeConfig
import scalikejdbc._

/**
  * The default NamedDb is provided as a case class, we need to provide our own
  * specialization interface for specialized (locked in) sub classes to implement.
  *
  * We can create instances to allow easy access to certain pools:
  *
  * {{{
  *   case class Master()(implicit context: ConnectionPoolContext = NoConnectionPoolContext) extends NamedDBConnection with DBRedisTransactions {
  *     override val name = 'master
  *   }
  *
  *   case class Replica()(implicit context: ConnectionPoolContext = NoConnectionPoolContext) extends NamedDBConnection with DBRedisTransactions {
  *     override val name = 'slave
  *   }
  * }}}
  *
  */
abstract class NamedDBConnection()(implicit context: ConnectionPoolContext = NoConnectionPoolContext) extends DBConnection with TypeSafeConfig {
  val name = getPoolName("application.db.rw.pool")

  protected[this] def connectionPool(): ConnectionPool = Option(context match {
    case NoConnectionPoolContext => ConnectionPool(name)
    case _: MultipleConnectionPoolContext => context.get(name)
    case _ => throw new IllegalStateException("Unknown connection pool context is specified.")
  }) getOrElse {
    throw new IllegalStateException("Connection pool is not yet initialized.")
  }

  override def connectionAttributes: DBConnectionAttributes = {
    connectionPool().connectionAttributes
  }

  private lazy val db: DB = {
    val cp = connectionPool()
    DB(cp.borrow(), connectionAttributes)
  }

  def toDB(): DB = db

  def conn: Connection = db.conn

  def getPoolName(path: String, default: Symbol = 'default): Symbol = {
    if (config.hasPath(path))
      Symbol(config.getString(path))
    else
      default
  }
}
