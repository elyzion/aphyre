package jp.aphyre.core.db.scalikejdbc

import java.util.UUID

import scalikejdbc.SQLSyntaxSupport

import scala.annotation.implicitNotFound
import scala.collection.concurrent.TrieMap
import scala.reflect.ClassTag

/**
  * This trait marks a class as a partitioned table.
  *
  * This should be extended by the class that will provide
  * the actual interface to the database,
  * since it extends SQLSyntaxSupport.
  *
  * {{{
  * class AccountTable(id: Option[UUID]) extends PgReplicationSupport[Account] with Partitionable[UUID, Account] {
  *   import jp.aphyre.db.helpers.TypeBinders._
  *   import jp.aphyre.core.db.scalikejdbc.DefaultPartitionIndexers.uuid
  *
  *   def partitionName(implicit p: Partition[UUID]): String = p.getPartitionName(id)("accounts")
  *
  *   override def tableName = partitionName
  *
  *   override val columns = Seq("id", "insert_timestamp", "update_timestamp")
  *
  *   def apply(u: SyntaxProvider[Account])(rs: WrappedResultSet): Account = apply(u.resultName)(rs)
  *   def apply(u: ResultName[Account])(rs: WrappedResultSet): Account = new Account(
  *     id = rs.get[UUID](u.id),
  *     insertTimestamp = rs.jodaDateTimeOpt(u.insertTimestamp),
  *     updateTimestamp = rs.jodaDateTimeOpt(u.updateTimestamp)
  *   )
  * }
  * }}}
  *
  * @tparam K The type of the key used to partition
  * @tparam V The record type, this is the case class for ScalikeJDBC
  */
trait Partitionable[K <: Any, V] extends SQLSyntaxSupport[V] {
  def partitionName(implicit p: Partition[K]): String
}

/**
  * Logic required for construction of partition names.
  *
  * Has to be implemented for the key type on which the partitioning will
  * be done. Tells the partitioning system how to create table names
  * and how to create the partition segments of the partitioned table.
  *
  * @tparam K The type of the key used to partition
  */
@implicitNotFound("Cannot find implicit instance of Partition for type ${K}")
trait Partition[K] {
  /**
    * Returns the partition index created from the provided id.
    *
    * This will, for example, return the last digit of a UUID or
    * a Long. Or it might return a YYYYmmdd formatted string for
    * a date based partition.
    *
    * @param id The value to calculate the partition from.
    * @return the partition string
    */
  def getIndex(id: K): String

  /**
    * Creates the table name including the partition
    * data.
    *
    * For example, this might create  a
    * @param id The value to calculate the partition from.
    * @return the partition name.
    */
  def getPartitionName(id: Option[K])(prefix: String): String = id match {
    case Some(i) ⇒ s"${prefix}_${getIndex(i)}"
    case None ⇒ prefix
  }
}

/**
  * Provides indexers for common use cases.
  *
  * Import into the scope where required.
  *
  * {{{
  *   implicit val indexer = jp.aphyre.core.db.scalikejdbc.DefaultPartitionIndexers.uuid
  * }}}
  */
object DefaultPartitionIndexers {
  implicit def uuid = new Partition[UUID] {
    def getIndex(id: UUID): String = id.toString.last.toString
  }

  implicit def long = new Partition[Long] {
    def getIndex(id: Long): String = (id % 10).toString
  }

  implicit def alphaNumeric = new Partition[String] {
    def getIndex(id: String): String = id.last.toString
  }

}

/**
  * Trait to be extended by the companion object for partitioned tables.
  *
  * When partitioning, the companion object does not extends SQLSyntax.
  * The companion object instead delegated all calls to the appropriate
  * Partitionable implementation.
  *
  * Not that the be DBSession is taken from lookupTable.
  * The table and the syntax are also created from looked up table.
  *
  * For more details please see the section on sharding/partitioning in http://scalikejdbc.org/documentation/sql-interpolation.html
  *
  * {{{
  *
  * object Account extends Partitioning[UUID, AccountTable] with InjectionSupport {
  *   // The indexer to use
  *   implicit val indexer = jp.aphyre.core.db.scalikejdbc.DefaultPartitionIndexers.uuid
  *
  *   def masterAutoSession: DBSession = lookupTable(None).masterAutoSession
  *   def autoSession: DBSession = lookupTable(None).autoSession

  *   val redis = inject[RedisClient]
  *   val system: ActorSystem = inject[ActorSystem]
  *   implicit val executionContext = system.dispatchers.lookup("akka.actor.business-logic-dispatcher")

  *   def apply(): Account = new Account(id = UUID.randomUUID(), insertTimestamp = None, updateTimestamp = None)
  *   def apply(id: String): Account = new Account(UUID.fromString(id), insertTimestamp = None, updateTimestamp = None)

  *   def find(id: UUID)(implicit session: DBSession = autoSession): Future[Option[Account]] = {
  *     // uses the implicit indexer to get the AccountTable instance.
  *     val t = lookupTable(Some(id))
  *     val u = t.syntax("u")
  *     redis.get[Account](AccountKey(id)) map {
  *       case Some(account) => Some(account)
  *       case None =>
  *         val account: Option[Account] = withSQL {
  *           select.from(t as u).where.eq(t.column.id, id)
  *         }.map(t(u.resultName)).single.apply()
  *         account foreach {
  *           case x => redis.set(AccountKey(id), x)
  *         }
  *         account
  *     }
  *   }
  * }
  * }}}
  *
  * @tparam K The type of the key used to partition
  * @tparam V The table type, this is the class that implements Partitionable.
  */
trait Partitioning[K <: Any, V <: Partitionable[_, _]]  {
  import scala.reflect._

  /**
    * Hold a map of partition table instances.
    */
  protected var partitions: TrieMap[String, V] = TrieMap.empty

  /**
    * Looks up the partition table for the given id.
    *
    * If an instance is not available a new instance will be inserted.
    * All operations are atomic. Alias for lookupTable(id: Option[K]).
    *
    * @param id The value to calculate the partition from.
    * @return
    */
  def lookupTable(id: K)(implicit ctk: ClassTag[K], ctv: ClassTag[V], p: Partition[K]): V = lookupPartition(Option(id))

  def lookupPartition(id: K)(implicit ctk: ClassTag[K], ctv: ClassTag[V], p: Partition[K]): V = lookupPartition(Option(id))

  /**
    * Looks up a table from our registered partitions.
    *
    * If an instance is not available a new instance will be inserted.
    * All operations are atomic.
    *
    * @param id The value to calculate the partition from.
    * @return
    */
  def lookupTable(id: Option[K])(implicit ctk: ClassTag[Option[K]], ctv: ClassTag[V], p: Partition[K]): V = {
    lookupPartition(id)
  }

  def lookupPartition(id: Option[K])(implicit ctk: ClassTag[Option[K]], ctv: ClassTag[V], p: Partition[K]): V = {
    val index = id match {
      case Some(i) ⇒ p.getIndex(i)
      case None ⇒ "none"
    }
    partitions.getOrElseUpdate(index , instantiate(id))
  }


  /**
    * Instantiates a partition from a partitionable table class V.
    *
    * @param id The value to calculate the partition from.
    * @return
    */
  private def instantiate(id: Option[K])(implicit ctk: ClassTag[Option[K]], ctv: ClassTag[V]): V =  {
    ctv.runtimeClass.getConstructors()(0).newInstance(id).asInstanceOf[V]
  }
}
