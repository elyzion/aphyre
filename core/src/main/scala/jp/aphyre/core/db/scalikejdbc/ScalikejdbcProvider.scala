package jp.aphyre.core.db.scalikejdbc

import jp.aphyre.core.db.DatabaseProvider
import scalikejdbc.config.DBs

/**
  * Basic database configuration.
  */
class ScalikejdbcProvider extends DatabaseProvider {
  def init(): Unit = {
    logger.info(s"Using default database implementation.")
    logger.info(s"Database: Starting dbs")
    DBs.setupAll()
  }
  def shutdown(): Unit = {
    logger.info(s"Database: Shutting down dbs")
    DBs.closeAll()
  }
}
