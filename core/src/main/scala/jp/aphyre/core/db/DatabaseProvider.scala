package jp.aphyre.core.db

import org.slf4j.{Logger, LoggerFactory}

/**
 * Trait for basic database configuration.
 */
trait DatabaseProvider {
  lazy val logger: Logger = LoggerFactory.getLogger(this.getClass)
  def init(): Unit
  def shutdown(): Unit
  def alive(): Boolean = true
}