package jp.aphyre.core.module

import _root_.akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import jp.aphyre.core.api.{EntryPoint, NullEntryPoint}
import jp.aphyre.core.db.DatabaseProvider
import jp.aphyre.core.db.scalikejdbc.ScalikejdbcProvider
import scaldi._


/**
  * Object to setup core/system scaldi modules.
  */
object DefaultModules {
  def modules: Injector = TypesafeConfigInjector() :: new CoreModule :: new DatabaseModule
}
