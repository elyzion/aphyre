package jp.aphyre.core.module

import akka.actor.ActorSystem
import jp.aphyre.core.api.{EntryPoint, NullEntryPoint}
import scaldi.Module

/**
  * Binds core system services.
  *
  * The actor systems bound here can be tuned in the application.conf using their names.
  */
class CoreModule extends Module {
  bind [ActorSystem] to ActorSystem("aphyre-rest") destroyWith (_.terminate())
  // Bind default API entry point to the NullEntryPoint implementation.
  bind [EntryPoint] to new NullEntryPoint
}
