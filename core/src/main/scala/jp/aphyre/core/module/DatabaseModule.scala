package jp.aphyre.core.module

import jp.aphyre.core.db.DatabaseProvider
import jp.aphyre.core.db.scalikejdbc.ScalikejdbcProvider
import scaldi.Module

/**
  * Bindings for the the default database implementation.
  * It is possible to override the mode argument for the module
  * to make use of different database environments.
  *
  * @param mode the default mode
  */
class DatabaseModule(mode: String = "dev") extends Module {
  bind [DatabaseProvider] to injected [ScalikejdbcProvider] initWith (_.init()) destroyWith (_.shutdown())
}
