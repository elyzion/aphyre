package jp.aphyre.core.error

/**
  * Implicit conversion to options for rest error types.
  * Import this into scope in order to make passing parameters to
  * rest errors simpler.
  *
  */
trait RestErrorImplicits {
  implicit def intToOptInt(arg: Int): Option[Int] = Option(arg)
  implicit def stringToOptString(arg: String): Option[String] = Option(arg)
  implicit def exceptionToOptException(arg: Exception): Option[Exception] = Option(arg)
  implicit def exceptionToOptString(arg: Exception): Option[String] = Option(arg.getMessage)
}

object RestErrorImplicits extends RestErrorImplicits