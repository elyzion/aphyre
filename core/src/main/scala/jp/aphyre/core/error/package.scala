package jp.aphyre.core

package object error {
  /**
    * Case class for debugging/verbose error responses.
    *
    * @param message The error message
    * @param cause The cause of the error
    */
  case class DebugErrorResponse(message: Option[String], cause: Option[Throwable])
}
