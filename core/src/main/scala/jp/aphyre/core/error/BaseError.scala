package jp.aphyre.core.error

import akka.http.scaladsl.model.{StatusCode, StatusCodes}

/**
  * Abstract parent class for all errors produced by the framework.
  *
  * Implement errors by extending this class. Expose any parameters that should be
  * overriddable by users. Pass along any parameters that cannot be overridden directly
  * to the parent class constructor.
  *
  * Example for a not found error:
  *
  * {{{
  *  case class NotFound(override val message: Option[String] = None,
  *                    override val cause: Option[Throwable] = None) extends
  *    BaseRestError(
  *      httpStatusCode = StatusCodes.NotFound,
  *      detailStatusCode = None,
  *      message = message,
  *      cause = cause
  *    )
  * }}}
  *
  * httpStatusCode is returned as the http status of the response. detailStatusCode
  * is returned in the `detail-status-code` custom header, if set.
  * message and cause are returned when verbose debugging reponses are enabled.
  *
  * @param httpStatusCode The spray.http.StatusCode to be returned for the error
  * @param detailStatusCode The detailed error code
  * @param message The debugging message
  * @param cause The cause of the error, if caused by an exception
  */
abstract class BaseError(val httpStatusCode: StatusCode = StatusCodes.InternalServerError,
                         val detailStatusCode: Option[Int] = None,
                         val message: Option[String] = None,
                         val cause: Option[Throwable] = None
                             ) extends Throwable(message.getOrElse(""), cause.getOrElse(new Exception()))
