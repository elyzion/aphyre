package jp.aphyre.core.error

import akka.event.LoggingAdapter
import akka.http.scaladsl.marshalling.{Marshaller, ToResponseMarshaller}
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler
import akka.util.ByteString
import jp.aphyre.core.json.Marshalling
import jp.aphyre.core.error.DebugErrorResponse

import scala.util.control.NonFatal

trait BaseErrorHandler extends Marshalling {
  import spray.json._

  /**
    * The header for detail status code.
    */
  val `detail-status-code` = "detail-status-code"

  /**
    * Implicit converter for
    *
    * @param pre
    * @param debug
    * @return
    */
  implicit def restErrorToHttpEntity(pre: BaseError)(implicit debug: Boolean = true): HttpEntity.Strict = {
    // TODO: デバッグモードの設定方法を追加します。。
    if (debug) {
      HttpEntity.Strict(
        ContentTypes.`application/json`,
        ByteString(DebugErrorResponse(message = pre.message, cause = pre.cause).toJson.prettyPrint)
      )
    } else  {
      HttpEntity.Empty
    }
  }


  implicit val RestErrorMarshaller: ToResponseMarshaller[BaseError] = Marshaller.withFixedContentType(MediaTypes.`application/json`) { value ⇒
    val detailHeader = value.detailStatusCode map { code ⇒ List(RawHeader(`detail-status-code`, code.toString)) }
    HttpResponse(status = value.httpStatusCode, entity = value, headers = detailHeader.getOrElse(List.empty))
  }

  /**
    *
    * @param log
    * @return
    */
  implicit def baseExceptionHandler(implicit log: LoggingAdapter): ExceptionHandler =
    ExceptionHandler {
      case NonFatal(restError: BaseError) =>
        log.error(restError,
          s"""Application return expected error status code ${restError.httpStatusCode} with:
             | DetailStatusCode: ${restError.detailStatusCode.map(_.toString).getOrElse("")}
             | Message: ${restError.message.getOrElse("")}
             | Additional details are logged with this entry.
           """.stripMargin)
        val response = restError.detailStatusCode match {
          case Some(code) ⇒
            HttpResponse(status = restError.httpStatusCode, entity = restError, headers = List(RawHeader(`detail-status-code`, code.toString)))
          case None ⇒
            HttpResponse(status = restError.httpStatusCode, entity = restError)
        }

        complete(response)
      case NonFatal(e) => // error from non-rest layer.
        log.error(e , s"Application encountered an unexpected error")
        // If we enable proper new relic support, we should call NewRelic.noticeError(e) here.
        complete((StatusCodes.InternalServerError, "Unknown server-side error"))
      case e: Exception ⇒ //critical error
        // TODO:
        log.error(e, s"Application encountered a critical error, and will shut down")
        complete((StatusCodes.InternalServerError, "Critical server-side error encountered."))
    }
}

object BaseErrorHandler extends BaseErrorHandler
