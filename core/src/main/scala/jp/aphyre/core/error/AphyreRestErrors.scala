package jp.aphyre.core.error

import akka.http.scaladsl.model.StatusCodes

/**
  * Collection of predefined error responses for the framework.
  *
  * TODO: add additional error types. Split errors into packages and classes by type/domain.
  */
object AphyreRestErrors {
  // @formatter:off

  /**
    * Internal Server Error
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class InternalServerError(override val message: Option[String] = None,
                      override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.InternalServerError,
      detailStatusCode = None,
      message = message,
      cause = cause
    )

  /**
    * Bad Request
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class BadRequest(override val message: Option[String] = None,
                        override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.BadRequest,
      detailStatusCode = None,
      message = message,
      cause = cause
    )

  /**
    * Resource not found
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class NotFound(override val message: Option[String] = None,
                      override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.NotFound,
      detailStatusCode = None,
      message = message,
      cause = cause
    )

  /**
    * Maintenance
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class Maintenance(override val message: Option[String] = None,
                         override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.ServiceUnavailable,
      detailStatusCode = Some(9000),
      message = message,
      cause = cause
    )

  /**
    * Timeout
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class ServiceTimeout(override val message: Option[String] = None,
                     override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.ServiceUnavailable,
      detailStatusCode = Some(9100),
      message = message,
      cause = cause
    )

  /**
    * Generic database error.
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class GenericDBError(override val message: Option[String] = None,
                            override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.InternalServerError,
      detailStatusCode = Some(9900),
      message = message,
      cause = cause
    )

  /**
    * Database connection error
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class DBConnectionError(override val message: Option[String] = None,
                               override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.InternalServerError,
      detailStatusCode = Some(9901),
      message = message,
      cause = cause
    )

  /**
    * SQL Error
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class SQLError(override val message: Option[String] = None,
                      override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.InternalServerError,
      detailStatusCode = Some(9902),
      message = message,
      cause = cause
    )

  /**
    * Authorization error
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class Unauthorized(override val message: Option[String] = None,
                          override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.Unauthorized,
      detailStatusCode = Some(1010),
      message = message,
      cause = cause
    )

  /**
    * Missing data error
    *
    * @param message The debugging message
    * @param cause The cause of the error, if caused by an exception
    */
  case class NoData(override val message: Option[String] = None,
                    override val cause: Option[Throwable] = None) extends
    BaseError(
      httpStatusCode = StatusCodes.BadRequest,
      detailStatusCode = Some(1000),
      message = message,
      cause = cause
    )
  // @formatter:on

}
