application {

  //dev, test or prod
  mode = "dev" //Development mode.
  mode = ${?APPLICATION_MODE}
  name = "AphyreRest"

  # Modules that require DB connection will look up the default ro and rw pools here.
  # the read only connection pool
  db.ro.pool = "default"
  # the read/write connection pool
  db.rw.pool = "default"

  //if true, starts up the kamon monitoring system.
  kamon {
    start = true
  }

  server {
    host = localhost
    port = 8080
    startupTimeout = 15
    timeout = 5
  }
}

dev {
  db.default.driver = "org.h2.Driver"
  db.default.url = "jdbc:h2:mem:test;MODE=PostgreSQL"
  db.default.user = "sa"
  db.default.password = ""
}

prod {
  db.default.driver = "org.h2.Driver"
  db.default.url = "jdbc:h2:mem:prod;MODE=PostgreSQL"
  db.default.user = "sa"
  db.default.password = ""
}


akka {
  loglevel = INFO
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
  logger-startup-timeout = 10s
  loggers-dispatcher = "akka.actor.logging-dispatcher"

  http {
    server {
      remote-address-header = on
      server-header = "Aphyre"
    }
  }

  stream {
    materializer {
      dispatcher = "akka.actor.akka-http-dispatcher"
    }
  }

  actor {

    # Dispatchers - Tune them to suit the application.
    akka-http-dispatcher {
      type = "Dispatcher"
      executor = "fork-join-executor"
      fork-join-executor {
        parallelism-min = 2
        parallelism-factor = 2.0
        parallelism-max = 24
      }
      throughput = 24
    }

    logging-dispatcher {
      type = "Dispatcher"
      executor = "fork-join-executor"
      fork-join-executor {
        parallelism-min = 2
        parallelism-factor = 2.0
        parallelism-max = 12
      }
      throughput = 8
    }

    business-logic-dispatcher {
      type = "Dispatcher"
      executor = "fork-join-executor"
      fork-join-executor {
        parallelism-min = 2
        parallelism-factor = 2.0
        parallelism-max = 12
        task-peeking-mode = "FIFO"
      }
      throughput = 8
    }
  }
}

# Connection Pool settings
db.default.poolInitialSize=5
db.default.poolMaxSize=7
db.default.poolConnectionTimeoutMillis=1000
db.default.poolValidationQuery="select 1 as one"
db.default.poolFactoryName="commons-dbcp"

scalikejdbc.global.loggingSQLAndTime.enabled = true
scalikejdbc.global.loggingSQLAndTime.logLevel = info
scalikejdbc.global.loggingSQLAndTime.warningEnabled = false
scalikejdbc.global.loggingSQLAndTime.warningThresholdMillis = 1000
scalikejdbc.global.loggingSQLAndTime.warningLogLevel = warn
scalikejdbc.global.loggingSQLAndTime.singleLineMode = true
scalikejdbc.global.loggingSQLAndTime.printUnprocessedStackTrace = false
scalikejdbc.global.loggingSQLAndTime.stackTraceDepth = 1

kamon {
  akka-http.client.instrumentation-level = "request-level"
  internal-config {
    akka {
      loglevel = INFO
    }
  }
}