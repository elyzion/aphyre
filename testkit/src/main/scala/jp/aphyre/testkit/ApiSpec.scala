package jp.aphyre.testkit

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import jp.aphyre.core._
import jp.aphyre.core.db.DatabaseProvider
import org.scalatest._
import scaldi.{Injectable, Injector}
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.http.scaladsl.server.Directives._
import akka.testkit.{ImplicitSender, TestKitBase}
import jp.aphyre.core.api.EntryPoint
import jp.aphyre.core.error.BaseErrorHandler
import jp.aphyre.core.module.DefaultModules

import scala.concurrent.duration.FiniteDuration

/**
 * Wraps together everything required for testing API interfaces.
 * Extends this class for your API tests.
 */
trait ApiSpec extends BaseApiSpec with BeforeAndAfterAll with BaseErrorHandler with Directives with DatabaseSpec with Matchers {
  this: Suite with Configurable =>

  import Injectable._

  implicit val logger = Logging(actorSystem, this.getClass.getCanonicalName.toString)
  override implicit def appModule: Injector = module :: DefaultModules.modules
  val router = inject [EntryPoint]
  val routes = router.route

  val customException = handleExceptions(baseExceptionHandler)
}

/**
 * http://doc.akka.io/docs/akka/snapshot/scala/testing.html#Asynchronous_Integration_Testing_with_TestKit
 *
 * Provides testing for akka actors.
 */
trait ActorSpec extends TestKitBase with BeforeAndAfterAll with ImplicitSender with DatabaseSpec with Matchers {
  this: Suite with Configurable =>

  import scaldi.akka.AkkaInjectable._
  override implicit def appModule: Injector = module :: DefaultModules.modules
  implicit lazy val system: ActorSystem = inject [ActorSystem]

  override def afterAll {
    shutdown(system)
  }
}

/**
 * Provides database awareness for tests.
 */
trait DatabaseSpec extends Matchers {
  this: Suite with Configurable =>
  import Injectable._

  implicit def appModule: Injector = module :: DefaultModules.modules

  implicit val _db = inject [DatabaseProvider]
}

/**
 * The base trait for testing API interfaces.
 */
trait BaseApiSpec extends ScalatestRouteTest {
  this: Suite =>
  val Ok = HttpResponse()
  val CompleteOk = complete(Ok)

  def echoComplete[T]: T => Route = { x => complete(x.toString) }
  def echoComplete2[T, U]: (T, U) => Route = { (x, y) => complete(s"$x $y") }

  implicit val routeTestTimeout: RouteTestTimeout = RouteTestTimeout(FiniteDuration(5, TimeUnit.SECONDS))

  implicit def actorSystem: ActorSystem = system
  def actorRefFactory = system
}
