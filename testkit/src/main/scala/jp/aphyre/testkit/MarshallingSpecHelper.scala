package jp.aphyre.testkit

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.{ActorMaterializer, Materializer}
import jp.aphyre.core.error.{BaseErrorHandler, BaseError}
import jp.aphyre.core.json.Marshalling
import org.scalatest.{FreeSpecLike, Matchers}
import org.scalatest.concurrent.ScalaFutures

/**
  * Created by elyzion on 4/27/16.
  */
abstract class MarshallingSpec extends FreeSpecLike with Marshalling with Matchers with ScalaFutures with BaseErrorHandler with ScalatestRouteTest {
  def preToHttpResponse(value: BaseError): HttpResponse = {
    val detailHeader = value.detailStatusCode map { t ⇒ List(RawHeader(`detail-status-code`, t.toString)) } getOrElse { List.empty }
    HttpResponse(status = value.httpStatusCode, entity = value, headers = detailHeader)
  }
}
