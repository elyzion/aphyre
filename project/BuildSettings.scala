import jp.aphyre.plugin.Dependencies
import sbt.Keys._
import sbt._

/**
  * General settings for the project.
  */
object BuildSettings {
  val buildName = "aphyre"
  val buildOrganization = "jp.aphyre"

  val buildSettings = Defaults.coreDefaultSettings ++ Seq(
    name := buildName,
    version := "0.0.1-SNAPSHOT",
    scalaVersion := Dependencies.Versions.scala212,
    organization := buildOrganization
  )
}

