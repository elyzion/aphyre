resolvers += "Flyway" at "https://flywaydb.org/repo"
libraryDependencies += "org.hsqldb" % "hsqldb" % "2.3.2"

lazy val root = (project in file(".")) dependsOn aphyrePlugin
lazy val aphyrePlugin = ProjectRef(file("../plugin"), "plugin")