package jp.aphyre.hikari

import com.typesafe.config.{Config, ConfigFactory}
import jp.aphyre.core.TypeSafeConfig

import scala.collection.JavaConverters._
import scala.collection.mutable

trait DatabaseConfig extends TypeSafeConfig {
  assert(Option(config).isDefined, s"Base configuration not found")
  assert(config.hasPath("db"), s"db section not found in configuration")
  assert(config.hasPath("db.template"), s"No configuration template found.l")

  val configTemplate: Config = config.getConfig("db.template")
  val dbConfig: Config = config.getConfig("db").resolve().withoutPath("template")
  val dbPoolNames: mutable.Set[String] = dbConfig.root.keySet.asScala
}