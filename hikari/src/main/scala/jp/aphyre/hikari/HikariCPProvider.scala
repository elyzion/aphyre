package jp.aphyre.hikari

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import jp.aphyre.core.db.DatabaseProvider
import scalikejdbc._
import scala.collection.JavaConverters._



/**
  * A replicating pool provider for MySQL.
  *
  * Please refer to the reference.conf for an example configuration.
  *
  */
class HikariCPProvider extends DatabaseProvider with DatabaseConfig {

  override def init(): Unit = {
    dbPoolNames.foreach(name => init(Symbol(name)))
  }

  override def alive(): Boolean = {
    try {
      dbPoolNames.forall { name =>
        NamedDB(Symbol(name)) localTx { implicit session =>
          sql"SELECT 1".execute().apply()
        }
      }
    } catch {
      case e: Exception =>
        logger.error("DB liveness check failed: " + e.getMessage + "\n" + e.getStackTrace.mkString("\n\t"))
        false
    }
  }

  private[this] def init(target: Symbol): Unit = {
    assert(dbConfig.hasPath(s"${target.name}"), s"No db configuration found for ${target.name}")
    val targetConfig = dbConfig.getConfig(s"${target.name}").withFallback(configTemplate)

    if (!ConnectionPool.isInitialized(target)) {
      logger.info(s"Using HikariCP based database implementation for ${target.name}")
      logger.info(s"Database: Starting db for ${target.name}")
      val hikariConfig: HikariConfig = new HikariConfig()

      if (targetConfig.hasPath("dataSourceClassName"))
        hikariConfig.setDataSourceClassName(targetConfig.getString("dataSourceClassName"))
      if (targetConfig.hasPath("user"))
        hikariConfig.addDataSourceProperty("user", targetConfig.getString("user"))
      if (targetConfig.hasPath("password"))
        hikariConfig.addDataSourceProperty("password", targetConfig.getString("password"))

      if (targetConfig.hasPath("jdbcUrl"))
        hikariConfig.setJdbcUrl(targetConfig.getString("jdbcUrl"))

      if (targetConfig.hasPath("driverClassName"))
        hikariConfig.setDriverClassName(targetConfig.getString("driverClassName"))

      // Configure connection pool specific settings.
      if (targetConfig.hasPath("pool")) {
        val poolConfig = targetConfig.getConfig("pool").withFallback(configTemplate.getConfig("pool"))
        if (poolConfig.hasPath("poolName"))
          hikariConfig.setPoolName(poolConfig.getString("poolName"))
        else
          hikariConfig.setPoolName(target.name)
        if (poolConfig.hasPath("minimumIdle"))
          hikariConfig.setMinimumIdle(poolConfig.getInt("minimumIdle"))
        if (poolConfig.hasPath("maximumPoolSize"))
          hikariConfig.setMaximumPoolSize(poolConfig.getInt("maximumPoolSize"))
        if (poolConfig.hasPath("connectionTimeout"))
          hikariConfig.setConnectionTimeout(poolConfig.getLong("connectionTimeout"))
        if (poolConfig.hasPath("idleTimeout"))
          hikariConfig.setIdleTimeout(poolConfig.getLong("idleTimeout"))
        if (poolConfig.hasPath("maxLifetime"))
          hikariConfig.setMaxLifetime(poolConfig.getLong("maxLifetime"))
        if (poolConfig.hasPath("readOnly"))
          hikariConfig.setReadOnly(poolConfig.getBoolean("readOnly"))
        if (poolConfig.hasPath("leakDetectionThreshold"))
          hikariConfig.setLeakDetectionThreshold(poolConfig.getLong("leakDetectionThreshold"))
        if (poolConfig.hasPath("prepStmtCacheSize"))
          hikariConfig.addDataSourceProperty("prepStmtCacheSize", poolConfig.getInt("prepStmtCacheSize"))
        if (poolConfig.hasPath("prepStmtCacheSqlLimit"))
          hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", poolConfig.getInt("prepStmtCacheSqlLimit"))
        if (poolConfig.hasPath("cachePrepStmts"))
          hikariConfig.addDataSourceProperty("cachePrepStmts", poolConfig.getBoolean("cachePrepStmts"))
        if (poolConfig.hasPath("useServerPrepStmts"))
          hikariConfig.addDataSourceProperty("useServerPrepStmts", poolConfig.getBoolean("useServerPrepStmts"))
        if (poolConfig.hasPath("transactionIsolation"))
          hikariConfig.addDataSourceProperty("transactionIsolation", poolConfig.getString("transactionIsolation"))
        if (poolConfig.hasPath("logger"))
          hikariConfig.addDataSourceProperty("logger", poolConfig.getBoolean("logger"))
        if (poolConfig.hasPath("profileSql"))
          hikariConfig.addDataSourceProperty("profileSql", poolConfig.getBoolean("profileSql"))
        if (poolConfig.hasPath("assureReadOnly"))
          hikariConfig.addDataSourceProperty("assureReadOnly", poolConfig.getBoolean("assureReadOnly"))
      }

      val ds: HikariDataSource = new HikariDataSource(hikariConfig)
      ConnectionPool.add(target, new DataSourceConnectionPool(ds))
    }
  }

  override def shutdown(): Unit = {
    dbConfig.root.keySet.asScala.foreach { name: String =>
      logger.info(s"Database: Shutting down db connection pool $name")
      ConnectionPool.get(Symbol(name)).dataSource.asInstanceOf[HikariDataSource].close()
      logger.info(s"Database: Shut down db connection pool $name")
    }
  }
}
