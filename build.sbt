import BuildSettings._
import jp.aphyre.plugin.Dependencies._

val commonDependencies = Seq(
  scalatest % "test",
  akkaTestKit % "test"
)

val redisDependencies = Seq(
  redisScala
)

val hikariDendencies = Seq(
  hikari
)

val coreDependencies = Seq(
  scalikejdbcTest % "test",
  akkaStreamTestkit % "test",
  akkaHttpTestkit % "test",
  h2 % "test",
  kamonCore,
  postgres,
  akkaActor,
  akkaLogging,
  akkaStream,
  akkaHttp,
  akkaHttpSprayJson,
  scalikejdbc,
  scalikejdbcConfig,
  scaldi,
  scaldiAkka,
  logback
)

val testkitDependencies = Seq(
  akkaTestKit,
  scalikejdbcTest,
  akkaHttpTestkit,
  scalatest
)

val commonSettings =  BuildSettings.buildSettings ++
  Seq(
    scalaVersion := Versions.scala212,
    libraryDependencies ++= commonDependencies
  )

val redisSettings =
  commonSettings ++
    Seq(
      name := buildName + "-redis",
      libraryDependencies ++= redisDependencies
    )

val hikariSettings =
  commonSettings ++
    Seq(
      name := buildName + "-hikari",
      libraryDependencies ++= hikariDendencies
    )

val coreSettings =
  commonSettings ++
    Seq(
      name := buildName + "-core",
      libraryDependencies ++= coreDependencies
    )

val testkitSettings =
  commonSettings ++
    Seq(
      name := buildName + "-testkit",
      libraryDependencies ++= testkitDependencies
    )

lazy val `aphyre-plugin` = project in file("plugin")

lazy val `aphyre-core` = (project in file("core"))
  .settings(coreSettings: _*)
  .enablePlugins(AphyrePlugin)

lazy val `aphyre-redis` = (project in file("redis"))
  .settings(redisSettings: _*)
  .dependsOn(`aphyre-core`)
  .enablePlugins(AphyrePlugin)

lazy val `aphyre-hikari` = (project in file("hikari"))
  .settings(hikariSettings: _*)
  .dependsOn(`aphyre-core`)
  .enablePlugins(AphyrePlugin)

lazy val `aphyre-testkit` = (project in file("testkit"))
  .settings(testkitSettings: _*)
  .dependsOn(`aphyre-core`)
  .enablePlugins(AphyrePlugin)

lazy val `aphyre` = (project in file("."))
  .settings(inThisBuild(Seq(
    packagedArtifacts := Map.empty
  )))
  .aggregate(`aphyre-testkit`, `aphyre-core`, `aphyre-redis`, `aphyre-hikari`, `aphyre-plugin`)
  .enablePlugins(AphyrePlugin)