organization  := "jp.aphyre"
name          := "aphyre-plugin"
sbtPlugin     := true
scalaVersion  := "2.12.4"
publishArtifact := true

sourceGenerators in Compile += Def.task {
  val d = (sourceManaged in Compile).value
  val v = version.value
  val n = name.value
  val file = d / "jp" / "aphyre" / "plugin" / "VersionInfo.scala"
  IO.write(file, """
                   |package jp.aphyre.plugin
                   |
                   |object VersionInfo {
                   |  val version = "%s"
                   |}
                   |""".stripMargin.format(v, n))
  Seq(file)
}

resolvers += "Flyway" at "https://davidmweber.github.io/flyway-sbt.repo"

addSbtPlugin("com.github.gseitz"  % "sbt-release"   % "1.0.7")
addSbtPlugin("com.scalapenos"     % "sbt-prompt"    % "1.0.2")
addSbtPlugin("org.flywaydb"       % "flyway-sbt"    % "4.2.0")