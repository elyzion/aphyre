package jp.aphyre.plugin

import sbt._

/**
 * Dependencies. Manage version variable tc here.
  *
  * TODO: Update dependencies.. Switch to Circe implementation for Json.
 */
object Dependencies {

  object Versions {
    val akka = "2.5.8"
    val akkaHttp = "10.0.11"
    val spray = "1.3.3"

    val circe = "0.8.0"
    val akkaHttpCirce = "1.17.0"
    
    val scalikejdbc = "3.0.1"
    val hikari = "2.6.2"
    val h2 = "1.4.190"
    val scalaTest = "3.0.1"
    val typesafeConfig = "1.3.0"
    val logback = "1.1.7"
    val postgres = "9.4.1208"
    val sprayJson = "1.3.2"
    val scaldi = "0.5.8"
    val scalamock = "3.6.0"
    val kamon = "0.6.7"
    val mariaDb = "2.0.2"

    //Plugins versions
    val sbtRelease = "1.0.5"
    val sbtRevolver = "0.8.0"
    val sbtAssembly = "0.14.2"
    val flyway = "4.2.0"
    val scalikeJdbcMapper = scalikejdbc
    val prompt = "0.2.1"
    val rediscala = "1.8.0"

    val scala212 = "2.12.4"
  }

  // Logging
  val logback = "ch.qos.logback" % "logback-classic" % Versions.logback

  // Akka
  val akkaActor = "com.typesafe.akka" %% "akka-actor" % Versions.akka
  val akkaTestKit = "com.typesafe.akka" %% "akka-testkit" % Versions.akka
  val akkaLogging = "com.typesafe.akka" %% "akka-slf4j" % Versions.akka
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % Versions.akka
  val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Versions.akka

  // Akka Http
  val akkaHttp = "com.typesafe.akka" %% "akka-http" % Versions.akkaHttp
  val akkaHttpTestkit = "com.typesafe.akka" %% "akka-http-testkit" % Versions.akkaHttp
  val akkaHttpCirce = "de.heikoseeberger"   %% "akka-http-circe" % Versions.akkaHttpCirce
  val akkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % Versions.akkaHttp
  
  // circe
  val circeNumbers = "io.circe" %% "circe-numbers" % Versions.circe
  val circeCore = "io.circe" %% "circe-core" % Versions.circe
  val circeParser = "io.circe" %% "circe-parser" % Versions.circe
  val circeGeneric = "io.circe" %% "circe-generic" % Versions.circe
  val circeGenricExtras = "io.circe" %% "circe-generic-extras" % Versions.circe
  val circeShapes = "io.circe" %% "circe-shapes" % Versions.circe
  val circeLiteral = "io.circe" %% "circe-literal" % Versions.circe

  // Scalikejdbc
  val scalikejdbc = "org.scalikejdbc" %% "scalikejdbc" % Versions.scalikejdbc
  val scalikejdbcConfig = "org.scalikejdbc" %% "scalikejdbc-config" % Versions.scalikejdbc
  val scalikejdbcTest = "org.scalikejdbc" %% "scalikejdbc-test" % Versions.scalikejdbc

  // Storage backends
  val hikari = "com.zaxxer" % "HikariCP" % Versions.hikari
  val h2 = "com.h2database" % "h2" % Versions.h2
  val redisScala =  "com.github.etaty" %% "rediscala" % Versions.rediscala
  val mariaDb = "org.mariadb.jdbc" % "mariadb-java-client" % Versions.mariaDb
  val postgres = "org.postgresql" % "postgresql" % Versions.postgres

  // General testing
  val scalatest = "org.scalatest" %% "scalatest" % Versions.scalaTest
  val scalamock = "org.scalamock" %% "scalamock-scalatest-support" % Versions.scalamock


  // Scaldi DI
  val scaldi = "org.scaldi" %% "scaldi" % Versions.scaldi
  val scaldiAkka = "org.scaldi" %% "scaldi-akka" % Versions.scaldi

  // Kamon
  val kamonCore = "io.kamon" %% "kamon-core" % Versions.kamon
  val kamonScala = "io.kamon" %% "kamon-scala" % Versions.kamon
  val kamonAkka = "io.kamon" %% "kamon-akka" % Versions.kamon
  val kamonAutoweave = "io.kamon" %% "kamon-autoweave" % Versions.kamon
  val kamonStatsd = "io.kamon" %% "kamon-statsd" % Versions.kamon
  val kamonAkkaHttp = "io.kamon" %% "kamon-akka-http" % "0.6.8"
  val kamonJdbc = "io.kamon" %% "kamon-jdbc" % Versions.kamon
  val kamonLogReport = "io.kamon" %% "kamon-log-reporter" % Versions.kamon
  val kamonSystemMetrics = "io.kamon" %% "kamon-system-metrics" % Versions.kamon
  

  //Plugin dependencies
  val sbtRelease = "com.github.gseitz" % "sbt-release" % Versions.sbtRelease
  val sbtRevolver = "io.spray" % "sbt-revolver" % Versions.sbtRevolver
  val sbtAssembly = "com.eed3si9n" % "sbt-assembly" % Versions.sbtAssembly
  val flyway = "org.flywaydb" % "flyway-sbt" % Versions.flyway
  val prompt = "com.scalapenos" % "sbt-prompt" % Versions.prompt
}
