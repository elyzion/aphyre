package jp.aphyre.plugin

import org.flywaydb.sbt.FlywayPlugin
import sbt.Keys._
import sbt._
import sbt.plugins.JvmPlugin


/**
  * Plugins for shared aphyre dependencies and settings.
  */
object AphyrePlugin extends AutoPlugin {
  override def trigger = allRequirements
  override def requires = JvmPlugin
  // These build settings will get added to any that uses this plugin.
  override def buildSettings: Seq[Setting[_]] = basebuildSettings
  // These settings will get added to any project that uses this plugin.
  override lazy val projectSettings = baseAphyreSettings

  val buildScalaVersion = Dependencies.Versions.scala212

  lazy val basebuildSettings: Seq[sbt.Def.Setting[_]] = Seq(
    scalaVersion := buildScalaVersion,
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature","-language:reflectiveCalls",
      "-language:implicitConversions", "-language:postfixOps", "-language:dynamics", "-language:higherKinds",
      "-language:existentials", "-language:experimental.macros"),
    javacOptions in compile ++= Seq("-source", "1.8", "-target", "1.8"),
    javacOptions in doc := Seq("-source", "1.8"),
    parallelExecution in Test := false,
    fork in Test := true,
    testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/test-reports"),
    scalacOptions in Test ++= Seq("-Yrangepos"),
    javaOptions in Test += "-Dnewrelic.environment=test"
  )

  lazy val baseAphyreSettings: Seq[sbt.Def.Setting[_]] =
     Seq(
        // Skip docs
        publishArtifact in(Compile, packageDoc) := false,
        sources in(Compile, doc) := Seq.empty,
        envVars in Test := Map("application.mode" -> "test"),
        publishMavenStyle := true,
        publishArtifact in Test := false,
        pomIncludeRepository := {
          repo => true
        },
        libraryDependencies ++= {
          val n = name.value
          if (n.startsWith("aphyre")) {
            Seq.empty
          } else {
            //Seq.empty
            Seq(
              "jp.aphyre" %% "aphyre-core" % VersionInfo.version
            )
          }
        },
        //We will always get update warning as some dependencies only exists in some repos. Suppress these warnings.
        logLevel in update := Level.Error
    )
}




