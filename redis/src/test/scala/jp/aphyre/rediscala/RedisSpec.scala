package jp.aphyre.rediscala

import akka.actor.ActorSystem
import akka.testkit.TestKitBase
import akka.util.{ByteString, Timeout}
import org.scalatest.{FreeSpec, Matchers}
import redis.RedisClient
import scaldi.{Injector, Module, TypesafeConfigInjector}

import scala.concurrent.Future
import scala.util.{Success, Try}

class RedisSpec extends FreeSpec with TestKitBase with Matchers {
  import scaldi.Injectable._

  import scala.concurrent.duration._

  implicit val executionContext = system.dispatcher
  implicit val timeout = Timeout(10 seconds)

  implicit def optionByteStringToString(in: Option[ByteString]): String = in.getOrElse(ByteString.empty).toString

  object Config extends RedisConfig
  implicit def appModule: Injector = new Module {
    bind [ActorSystem] to ActorSystem() destroyWith (_.terminate())
  } :: Config.redisModule :: TypesafeConfigInjector()

  override implicit lazy val system: ActorSystem = inject [ActorSystem]
  val redisClient = inject[RedisClient]

  "Redis transactions" - {
    "non-blocking" - {
      "default" - {
        "can commit" in {
          val result = Redis() tx { implicit tx =>
            tx.set("a", "1")
          }
          for {
            tx <- result
            res <- redisClient.get("a")
          } yield {
            tx === true
            res.get.utf8String === "1"
          }
        }
        "rolls back" in {
          redisClient.set("a", "2")
          try {
            an[Exception] should be thrownBy {
              Redis() tx { implicit tx =>
                tx.set("a", "1")
                throw new Exception("error")
              }
            }
          } catch {
            case e: Exception => e.getMessage shouldEqual "error"
          } finally {
            for {
              res <- redisClient.get("a")
            } yield {
              res.getOrElse(ByteString.empty).toString === "2"
            }
          }
        }
      }
      "future" - {
        "can commit" in {
          val result = Redis() futureTx { implicit tx =>
            tx.set("a", "1") // this gives us a Future[Boolean]
          }
          for {
            tx <- result
            res <- redisClient.get("a")
          } yield {
            tx === true
            res.getOrElse(ByteString.empty).toString === "1"

          }
        }
        "rolls back" in {
          redisClient.set("a", "2")
          Redis() futureTx { implicit tx =>
            tx.set("a", "1")
            Future.failed(new Exception)
          }
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "2"
          }
        }
      }
      "either" - {
        "can commit" in {
          val result = Redis() eitherTx { implicit tx =>
            tx.set("a", "1")
            Right("ok!")
          }
          result shouldBe Right("ok!")
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "1"
          }
        }
        "rolls back" in {
          redisClient.set("a", "2")
          val result = Redis() eitherTx { implicit tx =>
            tx.set("a", "1")
            Left("ng!")
          }
          result shouldBe Left("ng!")
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "2"
          }
        }
      }
      "try" - {
        "can commit" in {
          val result = Redis() tryTx { implicit tx =>
            Try {
              tx.set("a", "1")
              "ok!"
            }
          }
          result shouldBe Success("ok!")
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "1"
          }
        }
        "rolls back" in {
          redisClient.set("a", "2")
          Redis() tryTx { implicit tx =>
            Try {
              tx.set("a", "1")
              throw new Exception("error")
            }
          }
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "2"
          }
        }
      }
    }
    "blocking" - {
      "default" - {
        "can commit" in {
          import RedisTxBoundary.BlockingException._
          val result = Redis() tx { implicit tx =>
            tx.set("a", "1")
          }
          for {
            tx <- result
            res <- redisClient.get("a")
          } yield {
            tx === true
            res.getOrElse(ByteString.empty).toString === "1"
          }
        }
        "rolls back" in {
          import RedisTxBoundary.BlockingException._
          redisClient.set("a", "2")
          try {
            an[Exception] should be thrownBy {
              Redis() tx { implicit tx =>
                tx.set("a", "1")
                throw new Exception("error")
              }
            }
          } catch {
            case e: Exception => e.getMessage shouldEqual "error"
          } finally {
            for {
              res <- redisClient.get("a")
            } yield {
              res.getOrElse(ByteString.empty).toString === "2"
            }
          }
        }
      }
      "future" - {
        "can commit" in {
          import RedisTxBoundary.BlockingFuture._
          val result = Redis() futureTx { implicit tx =>
            tx.set("a", "1") // this gives us a Future[Boolean]
          }
          for {
            tx <- result
            res <- redisClient.get("a")
          } yield {
            tx === true
            res.getOrElse(ByteString.empty).toString === "1"

          }
        }
        "rolls back" in {
          import RedisTxBoundary.BlockingFuture._
          redisClient.set("a", "2")
          Redis() futureTx { implicit tx =>
            tx.set("a", "1")
            Future.failed(new Exception)
          }
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "2"
          }
        }
      }
      "either" - {
        "can commit" in {
          import RedisTxBoundary.BlockingEither._
          val result = Redis() eitherTx { implicit tx =>
            tx.set("a", "1")
            Right("ok!")
          }
          result shouldBe Right("ok!")
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "1"
          }
        }
        "rolls back" in {
          import RedisTxBoundary.BlockingEither._
          redisClient.set("a", "2")
          val result = Redis() eitherTx { implicit tx =>
            tx.set("a", "1")
            Left("ng!")
          }
          result shouldBe Left("ng!")
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "2"
          }
        }
      }
      "try" - {
        "can commit" in {
          import RedisTxBoundary.BlockingTry._
          val result = Redis() tryTx { implicit tx =>
            Try {
              tx.set("a", "1")
              "ok!"
            }
          }
          result shouldBe Success("ok!")
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "1"
          }
        }
        "rolls back" in {
          import RedisTxBoundary.BlockingTry._
          redisClient.set("a", "2")
          Redis() tryTx { implicit tx =>
            Try {
              tx.set("a", "1")
              throw new Exception("error")
            }
          }
          for {
            res <- redisClient.get("a")
          } yield {
            res.getOrElse(ByteString.empty).toString === "2"
          }
        }
      }
    }
  }
}
