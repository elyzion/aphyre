package jp.aphyre.rediscala

import org.slf4j.LoggerFactory
import redis.commands.{TransactionBuilder, Transactions}
import redis.{RedisClient, RedisCommands}
import scaldi.Injector

import scala.concurrent.{Await, ExecutionContext, Future, Promise}
import scala.util.control.ControlThrowable
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._



/**
  *
  * Transactional wrapping for redis.
  * This allows us to mix redis and database transactions.
  *
  * The redis transaction will usually be inside the DB transaction, but either way is fine.
  * {{{
  *
  * Master() localTx { implicit session =>
  *   Redis() tx { implicit tx =>
  *     // watch the key to prevent concurrent modifications,
  *     // committing when the watched key has changed will result in an exception + cache/db rollback.
  *     tx.watch("a")
  *     // do db stuff.
  *     // PLEASE NOTE, CACHE UPDATE SHOULD BE DONE IN THE MODEL AS WELL.
  *     // This example writes the cache updates here for illustration purposes only.
  *     MyModel.insert(....)
  *     // update cache
  *     tx.set("a", a)
  *
  *   }
  *  }
  *
  * }}}
  *
  * Sentinel, Pool, MasterSlavePool implementation can be used by overriding the default RedisClient scaldi binding.
  *
  */
object Redis {
  import scaldi.Injectable._
  def apply()(implicit inj: Injector): Redis[_] = {
    // Remember, overriding the RedisClient binding will allow for using sentinels and pools.
    new Redis(inject [RedisClient])
  }
}


/**
  * Basic single client redis cache implementation.
  */
class Redis[+R <: RedisCommands with Transactions](redisClient: R) {
  val logger = LoggerFactory.getLogger(this.getClass)

  private[this] def defaultTxBoundary[A]: RedisTxBoundary[A] = RedisTxBoundary.Exception.exceptionTxBoundary[A]

  private[this] def rollbackIfThrowable[A](f: => A)(implicit tx: TransactionBuilder): A = {
    try {
      f
    } catch {
      case e: ControlThrowable =>
        // Control flow exception; these are not rolled back. Need to commit it here.
        tx.exec()
        throw e
      case originalException: Throwable =>
        try {
          tx.discard()
        } catch {
          case rollbackException: Throwable => logger.error("Could not successfully redis transaction", rollbackException)
        }
        throw originalException
    }
  }

  /**
    * Provides non-blocking local-tx session block. Will rollback on exception.
    *
    * Change the defaultTxBoundary by importing one of the tx boundaries from RedisTxBoundary.
    * This works the same as for transactions in ScalikeJDBC. Redis wont wait for the transaction
    * to commit before returning.
    *
    * @param execution the block of code to execute.
    * @tparam A  return type
    * @return result value
    */
  def tx[A](execution: TransactionBuilder => A)(implicit boundary: RedisTxBoundary[A] = defaultTxBoundary[A]): A = {
    implicit val tx = redisClient.transaction()
    rollbackIfThrowable[A] {
      val result: A = execution(tx)
      boundary.finishTx(result, tx)
      result
    }
  }

  /**
    * Transaction that will rollback on a Left.
    *
    * This is non-blocking by default.
    *
    * @param execution the block of code to execute.
    * @tparam L
    * @tparam R
    * @return
    */
  def eitherTx[L, R](execution: TransactionBuilder => Either[L, R]): Either[L, R] = {
    import RedisTxBoundary.Either._
    // Explicitly specify the implicits to make it slightly easier to read for new people.
    tx(execution)(eitherTxBoundary[L, R])
  }

  /**
    * Transaction that will rollback on a Failed future.
    *
    * This is non-blocking by default.
    *
    * @param execution the block of code to execute.
    * @param ec
    * @tparam A
    * @return
    */
  def futureTx[A](execution: TransactionBuilder => Future[A])(implicit ec: ExecutionContext): Future[A] = {
    import RedisTxBoundary.Future._
    // Explicitly specify the implicits to make it slightly easier to read for new people.
    tx(execution)(futureTxBoundary[A])
  }

  /**
    * Transaction that will rollback on a failed try.
    *
    * This is non-blocking by default.
    *
    * @param execution the block of code to execute.
    * @tparam A
    * @return
    */
  def tryTx[A](execution: TransactionBuilder => Try[A]): Try[A] = {
    import RedisTxBoundary.Try._
    // Explicitly specify the implicits to make it slightly easier to read for new people.
    tx(execution)(tryTxBoundary[A])
  }

}
/**
  * This type class enable users to customize the behavior of transaction boundary(commit/rollback).
  */
trait RedisTxBoundary[A] {

  /**
    * Finishes the current transaction.
    */
  def finishTx(result: A, tx: TransactionBuilder): A
}

/**
  * Contains redis transaction boundaries.
  *
  * Import the boundary you want into scope to use it.
  * {{{
  *   import RedisTxBoundary.Either._
  * }}}
  */
object RedisTxBoundary {

  /**
    * Exception RedisTxBoundary type class instance.
    */
  object Exception {
    implicit def exceptionTxBoundary[A] = new RedisTxBoundary[A] {
      def finishTx(result: A, tx: TransactionBuilder): A = {
        tx.exec()
        result
      }
    }
  }

  /**
    * Blocking Exception RedisTxBoundary type class instance.
    */
  object BlockingException {
    implicit def exceptionTxBoundary[A] = new RedisTxBoundary[A] {
      def finishTx(result: A, tx: TransactionBuilder): A = {
        Await.result(tx.exec(), 30 seconds)
        result
      }
    }
  }

  /**
    * Applies an operation to finish current transaction to the result.
    * When the operation throws some exception, the exception will be returned without fail.
    */
  private def doFinishTx[A](result: Try[A])(doFinish: Try[A] => Unit): Try[A] =
    scala.util.Try(doFinish(result)).transform(
      _ => result,
      finishError =>
        Failure(result match {
          case Success(_) => finishError
          case Failure(resultError) =>
            resultError.addSuppressed(finishError)
            resultError
        })
    )

  /**
    * Applies an operation to finish current transaction to the Future value which holds the result.
    * When the operation throws some exception, the exception will be returned without fail.
    */
  private def onFinishTx[A](resultF: Future[A])(doFinish: Try[A] => Unit)(implicit ec: ExecutionContext): Future[A] = {
    val p = Promise[A]
    resultF.onComplete(result => p.complete(doFinishTx(result)(doFinish)))
    p.future
  }

  /**
    * Future RedisTxBoundary type class instance.
    */
  object Future {
    implicit def futureTxBoundary[A](implicit ec: ExecutionContext) = new RedisTxBoundary[Future[A]] {
      def finishTx(result: Future[A], tx: TransactionBuilder): Future[A] = {
        onFinishTx(result) {
          case Success(_) => tx.exec()
          case Failure(_) => tx.discard()
        }
      }
    }
  }

  /**
    * Blocking Future RedisTxBoundary type class instance.
    *
    * This will block until the redis commit returns. The future
    */
  object BlockingFuture {
    implicit def futureTxBoundary[A](implicit ec: ExecutionContext) = new RedisTxBoundary[Future[A]] {
      def finishTx(result: Future[A], tx: TransactionBuilder): Future[A] = {
        onFinishTx(result) {
          case Success(_) => Await.result(tx.exec(), 30 seconds)
          case Failure(_) => tx.discard()
        }
      }
    }
  }

  /**
    * Either RedisTxBoundary type class instance.
    *
    * NOTE: Either RedisTxBoundary may throw an Exception when commit/rollback operation fails.
    */
  object Either {
    implicit def eitherTxBoundary[L, R] = new RedisTxBoundary[Either[L, R]] {
      def finishTx(result: Either[L, R], tx: TransactionBuilder): Either[L, R] = {
        result match {
          case Right(_) => tx.exec()
          case Left(_) => tx.discard()
        }
        result
      }
    }
  }

  /**
    * Blocking Either RedisTxBoundary type class instance.
    *
    * This will block until the redis commit returns.
    */
  object BlockingEither {
    implicit def eitherTxBoundary[L, R] = new RedisTxBoundary[Either[L, R]] {
      def finishTx(result: Either[L, R], tx: TransactionBuilder): Either[L, R] = {
        result match {
          case Right(_) => Await.result(tx.exec(), 30 seconds)
          case Left(_) => tx.discard()
        }
        result
      }
    }
  }

  /**
    * Try RedisTxBoundary type class instance.
    */
  object Try {

    implicit def tryTxBoundary[A] = new RedisTxBoundary[Try[A]] {
      def finishTx(result: Try[A], tx: TransactionBuilder): Try[A] = {
        doFinishTx(result) {
          case Success(_) => tx.exec()
          case Failure(_) => tx.discard()
        }
      }
    }
  }

  /**
    * Blocking Try RedisTxBoundary type class instance.
    *
    * This will block until the redis commit returns.
    */
  object BlockingTry {
    implicit def tryTxBoundary[A] = new RedisTxBoundary[Try[A]] {
      def finishTx(result: Try[A], tx: TransactionBuilder): Try[A] = {
        doFinishTx(result) {
          case Success(_) => Await.result(tx.exec(), 30 seconds)
          case Failure(_) => tx.discard()
        }
      }
    }
  }
}
