package jp.aphyre.rediscala

import akka.actor.ActorSystem
import redis.RedisClient
import scaldi.{Injector, Module}

trait RedisConfig {

  /**
    * DI binding for the basic redis client. Override this in your app to customize.
    *
    * @return
    */
  def redisModule: Injector = new Module {
    // Most basic redis client binding. Single client, no pooling. no sentinel.
    bind [RedisClient] to RedisClient(
      host = inject [String] ("application.redis.host" is by default "127.0.0.1"),
      port = inject [Int] ("application.redis.port" is by default 6379),
      password = Some(inject [String] ("application.redis.password" is by default "aphyreredis"))
    )(_system = inject [ActorSystem])
  }

}



