package jp.aphyre.rediscala

import jp.aphyre.core.db.scalikejdbc.NamedDBConnection
import redis.commands.TransactionBuilder
import scaldi.Injector
import scalikejdbc.{DBSession, TxBoundary}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Provides redis transaction integration for Scalikejdbc database transactions.
  *
  * Writing
  * {{{
  *   NamedDbConnection() localTx { implicit session =>
  *     RedisCache() tx { implicit tx =>
  *       //Stuff!!
  *     }
  *   }
  * }}}
  * can become tedious, so this wrapper is here to help!
  * Now we can write the above as:
  * {{{
  *   NamedDbConnection() localTxDBRedis { implicit session => implicit tx =>
  *     //Stuff!!!
  *   }
  * }}}
  */
trait ScalikejdbcRedisTransactionSupport {
  this: NamedDBConnection => // requires a NamedDbConnection
  /**
    * Provides default TxBoundary type class instance.
    */
  private[this] def defaultTxBoundary[A]: TxBoundary[A] = TxBoundary.Exception.exceptionTxBoundary[A]

  /**
    * Provides default RedisTxBoundary type class instance.
    */
  private[this] def defaultRedisTxBoundary[A]: RedisTxBoundary[A] = RedisTxBoundary.Exception.exceptionTxBoundary[A]


  /**
    * @param execution the curried partial function to execute.
    * @param boundary the txboundary for scalikejdbc. will use inscope implicit or default.
    * @param redisBoundary the txboundary for redis. will use inscope implicit or default.
    * @param inj the injector required for redis.
    * @tparam A
    * @return
    */
  def localTxDBRedis[A](execution: DBSession => TransactionBuilder => A)(
    implicit boundary: TxBoundary[A] = defaultTxBoundary[A],
    redisBoundary: RedisTxBoundary[A] = defaultRedisTxBoundary[A],
    inj: Injector): A = {
    localTx { implicit session =>
      Redis() tx { implicit tx =>
        execution(session)(tx)
      }
    }
  }

  /**
    *
    * @param execution  the curried partial function to execute.
    * @param ec implicit execution context required for futures.
    * @param inj injector required by redis
    * @tparam A
    * @return
    */
  def futureLocalTxDBRedis[A](execution: DBSession => TransactionBuilder => Future[A])(implicit ec: ExecutionContext, inj: Injector): Future[A] = {
    futureLocalTx  { implicit session =>
      Redis() futureTx  { implicit tx =>
        execution(session)(tx)
      }
    }
  }
}


